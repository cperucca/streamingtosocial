#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import urllib2
import urllib
import requests
import json
import ast
import logging
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

def Get_To_Wowza( url_per_wowza ):

	print " ---------------------- INIZIO Get_To_Wowza  ------------------ " 

	'''
	meta =  {
		'id': broadcast_id,
		'streamId': stream_id
	}

	param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

	'''
	meta = {}
	param = {}
	headers =  { 'Accept': 'application/json'}

	requ = requests.request('GET', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))
	
	# bind your stream
	print '\r\n'
	result = json.loads( requ.text )
	print json.dumps(result, indent=4)
	print " ---------------------- FINE Get_To_Wowza  ------------------ " 
	return


def Get_Wowza_Mapentry_Details( mapentry_name ):

	print " --------------------- WOWZA - INIZIO Get_Wowza_Mapentry_Details  ------------------ " 
	result = []

	'''
	meta =  {
		'id': broadcast_id,
		'streamId': stream_id
	}

	param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

	'''
	url_per_wowza = 'http://rsis-tifone-t:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/__APP_NAME__/pushpublish/mapentries/__MAP_ENTRY_NAME__' 
	url_per_wowza = url_per_wowza.replace('__APP_NAME__', 'live').replace('__MAP_ENTRY_NAME__', mapentry_name)

	meta = {}
	param = {}
	headers =  { 'Accept': 'application/json'}

	requ = requests.request('GET', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))
	
	# bind your stream
	print '\r\n'
	result = json.loads( requ.text )
	print json.dumps(result, indent=4)
	print " --------------------- WOWZA - FINE Get_Wowza_Mapentry_Details  ------------------ " 
	return result




def Delete_Mapentry( entry_name ):

	print " -------------------- WOWZA - INIZIO Delete_Mapentry  ------------------ "
	result = []

	url_per_wowza = 'http://rsis-tifone-t.media.int:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/__APP_NAME__/pushpublish/mapentries/__MAP_ENTRY_NAME__'
	url_per_wowza = url_per_wowza.replace('__APP_NAME__', 'live').replace('__MAP_ENTRY_NAME__', entry_name)

	'''
	Example:
        meta =  {
                'id': broadcast_id,
                'streamId': stream_id
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''

	# metto il template modificato come body della post per creare un nuovo stream_target
	# con i valori di default presi dal template; in caso volessi modificarli devo fare
	# le replace qui oppure modificare il template direttamente
        meta =  {} 
        param = {}
        headers =  { 'Accept': 'application/json',
			'Content-Type' : 'application/json',
			'Connection' : 'keep-alive'}

        requ = requests.request('DELETE', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " --------------------- WOWZA - FINE Delete_Mapentry  ------------------ "
        return result

def Action_Mapentry( entry_name, action ):

	# where action can be : copy - disable - enable - restart

	print " --------------------- WOWZA - INIZIO Action_Mapentry  -------- con action =  " + action
	result = []

	url_per_wowza = 'http://rsis-tifone-t.media.int:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/__APP_NAME__/pushpublish/mapentries/__MAP_ENTRY_NAME__/actions/__ACTION__'
	url_per_wowza = url_per_wowza.replace('__APP_NAME__', 'live').replace('__MAP_ENTRY_NAME__', entry_name).replace('__ACTION__', action )

	'''
        meta =  {
                'id': broadcast_id,
                'streamId': stream_id
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
	# metto il template modificato come body della post per creare un nuovo stream_target
	# con i valori di default presi dal template; in caso volessi modificarli devo fare
	# le replace qui oppure modificare il template direttamente
        meta =  {} 
        param = {}
        headers =  { 'Accept': 'application/json',
			'Content-Type' : 'application/json',
			'Connection' : 'keep-alive'}

        requ = requests.request('PUT', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " -------------------- WOWZA - FINE Action_Mapentry  ------------------ "
        return result

def Create_Mapentry_YT( entry_name, stream_name ):

	print " ---------------------- INIZIO Create_Mapentry YT  ------------------ "
	result = []

	url_per_wowza = 'http://rsis-tifone-t.media.int:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/__APP_NAME__/pushpublish/mapentries/__MAP_ENTRY_NAME__'
	url_per_wowza = url_per_wowza.replace('__APP_NAME__', 'live').replace('__MAP_ENTRY_NAME__', entry_name)

	# apro il json template da mendare come body_
	# il path sara cambiato con 
	#f_template = codecs.open(os.environ['WOW_RESOURCE_DIR'], 'r' , 'utf-8')

	f_template = codecs.open('/home/perucccl/StreamingToSocial/WOW/RESOURCES/mapentry_body_json_template_youtube', 'r' , 'utf-8')
	mapentry_body_json_template = json.load( f_template )
	# devo cambiare le entry streamName e entryName con i valori giusti
	mapentry_body_json_template['streamName'] = stream_name
	mapentry_body_json_template['entryName'] = entry_name

	print json.dumps( mapentry_body_json_template, indent=4)

	'''
        meta =  {
                'id': broadcast_id,
                'streamId': stream_id
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
	# metto il template modificato come body della post per creare un nuovo stream_target
	# con i valori di default presi dal template; in caso volessi modificarli devo fare
	# le replace qui oppure modificare il template direttamente
        meta =  mapentry_body_json_template 
        param = {}
        headers =  { 'Accept': 'application/json',
			'Content-Type' : 'application/json',
			'Connection' : 'keep-alive'}

        requ = requests.request('POST', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Create_Mapentry YT  ------------------ "
        return result



def Create_Mapentry_FB( entry_name, stream_name ):

	print " ---------------------- INIZIO Create_Mapentry FB ------------------ "
	result = []

	url_per_wowza = 'http://rsis-tifone-t.media.int:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/__APP_NAME__/pushpublish/mapentries/__MAP_ENTRY_NAME__'
	url_per_wowza = url_per_wowza.replace('__APP_NAME__', 'live').replace('__MAP_ENTRY_NAME__', entry_name)

	# apro il json template da mendare come body_
	# il path sara cambiato con 
	#f_template = codecs.open(os.environ['WOW_RESOURCE_DIR'], 'r' , 'utf-8')

	f_template = codecs.open('/home/perucccl/StreamingToSocial/WOW/RESOURCES/mapentry_body_json_template_generic_rtmp', 'r' , 'utf-8')
	mapentry_body_json_template = json.load( f_template )
	# devo cambiare le entry streamName e entryName con i valori giusti
	mapentry_body_json_template['streamName'] = stream_name
	mapentry_body_json_template['entryName'] = entry_name

	print json.dumps( mapentry_body_json_template, indent=4)

	'''
        meta =  {
                'id': broadcast_id,
                'streamId': stream_id
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
	# metto il template modificato come body della post per creare un nuovo stream_target
	# con i valori di default presi dal template; in caso volessi modificarli devo fare
	# le replace qui oppure modificare il template direttamente
        meta =  mapentry_body_json_template 
        param = {}
        headers =  { 'Accept': 'application/json',
			'Content-Type' : 'application/json',
			'Connection' : 'keep-alive'}

        requ = requests.request('POST', url_per_wowza ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Create_Mapentry FB ------------------ "
        return result

if __name__ == "__main__":

	# get list of live stream sources (applications)
	#Get_To_Wowza( 'http://rsis-tifone-t:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications')
	# per prendere gli Streams Target
	#Get_To_Wowza( 'http://rsis-tifone-t:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/pushpublish/mapentries')

	#Get_To_Wowza( 'http://rsis-tifone:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications')
	#Get_To_Wowza( 'http://rsis-tifone:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications/live/pushpublish/mapentries')

	#Create_Mapentry( 'Creata da pyWow', "1470954469602934?ds=1&s_l=1&a=ATgmR9IbjY4EhgoF" )
	Get_Wowza_Mapentry_Details( 'Creata da pyWow')
	Delete_Mapentry( 'Creata da pyWow' )
	#Action_Mapentry( 'Test_streaming_Anna_Pubblica' , 'disable')
	#Action_Mapentry( 'Test_create_facebook_Anna_Pubblica' , 'enable')
	#Action_Mapentry( 'test_YT_Create_Procedure' , 'enable')

