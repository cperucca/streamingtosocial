
import urllib2
import urllib
import requests
import json
import ast
import logging
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def Bind_LiveBroadcast( broadcast_id, stream_id, access_token ):

	print " ---------------------- INIZIO Bind_LiveBroadcast  ------------------ " 

	meta =  {
		'id': broadcast_id,
		'streamId': stream_id
	}

	param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	requset = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveBroadcasts/bind',headers=headers,params=param,data=json.dumps(meta))
	
	# bind your stream
	print requset.content
	return

	# get youtube id
	cont = json.loads(requset.content)            
	youtube_id = cont['id']
	
	print " ---------------------- FINE Bind_LiveBroadcast  ------------------ " 


def List_Applications(  ):
	# curl -X GET --header 'Accept:application/json; charset=utf-8'
	# http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications
	
	meta =  {
	} 

	param = { }


	headers =  {'Accept': 'application/json; charset-utf-8',
		   'Content-type': 'application/json'}

	requset = requests.request('POST', 'http://rsis-tifone-t:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications',headers=headers,params=param,data=json.dumps(meta))

	
	print requset.status_code
	print requset.content

	
	print " ---------------------- FINE List_Applications ------------------ " 

def dummy( title, desc, cdn_format, cdn_ingtype, cdn_res, cdn_fps, access_token ):
	# curl -X GET --header 'Accept:application/json; charset=utf-8'
	# http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications
	
	meta =  {
	  'snippet': {
	    'title': title,
	    'description': desc
	  },
	  'cdn': {
	    'format': cdn_format,
	    'ingestionType': cdn_ingtype
	    },
	    'resolution': cdn_res,
	    'frameRate': cdn_fps
	} 

	param = { 'part': 'snippet,cdn,status'}


	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	requset = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveStreams',headers=headers,params=param,data=json.dumps(meta))

	
	#upload your video
	print requset.status_code
	print requset.content

	# get youtube id
	cont = json.loads(requset.content)            
	youtube_id = cont['id']
	print youtube_id
	cdn_info = cont['cdn']
	ingestion_info = cdn_info['ingestionInfo']
	print ingestion_info
	return [youtube_id, ingestion_info]
	
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 

	
def Delete_LiveBroadcast( id, access_token ):

	print " ---------------------- INIZIO Delete_LiveBroadcast  ------------------ " 
	'''
	mandatory:
	'id': id
	'''

	param = { 'id': id }


	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	requset = requests.request('DELETE', 'https://www.googleapis.com/youtube/v3/liveBroadcasts',headers=headers,params=param)
	
	#upload your stream
	print requset.status_code
	print requset.content

	print " ---------------------- FINE Delete_LiveBroadcast ------------------ " 

def Trova_LiveBroadcast_Id_Dal_Nome( nome, access_token):

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	campo = 'liveBroadcasts?part=snippet,id,contentDetails,status,statistics&mine=true&access_token=' + access_token
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	return Trova_Id_Json_LiveBroadcast( result_dict , nome)

def Delete_LiveBroadcast_by_Name( name, access_token ):
	
	
	print " ---------------------- INIZIO Delete_LiveBroadcast_by_Name ------------------ " 
	# trova id del livebroadcast dato il nome
	livebroacast_id = Trova_LiveBroadcast_Id_Dal_Nome( name, access_token )
	if livebroacast_id:
		Delete_LiveBroadcast( livebroacast_id, access_token )	
		
	else:
		print ' Nome del Broadcast NOT FOUND'

	print " ---------------------- FINE Delete_LiveBroadcast_by_Name ------------------ " 

def Upload_Video( filename, title, description, tags, category, access_token ):
	# categoryId is '1' for Film & Animation
	# to fetch all categories: https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode={2 chars region code}&key={app key}

	meta =  {'snippet': {'categoryId': '24',
	  'description': 'description',
	  'tags': ['test1', 'test2'],
	  'title': title },
	  'status': {'privacyStatus': 'public'}}

	param = { 'part': 'snippet,status',
		 'uploadType': 'resumable'}

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	#get location url
	retries = 0
	retries_count = 1
	while retries <= retries_count: 
	    requset = requests.request('POST', 'https://www.googleapis.com/upload/youtube/v3/videos',headers=headers,params=param,data=json.dumps(meta))
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	if requset.status_code != 200:
	    print 'requset.status_code != 200:'
	    #do something

	location = requset.headers['location']
	file_name = '/Users/perucccl/Downloads/VID_20160909_122114.mp4'

	file_data = open(file_name, 'rb').read()

	headers =  {'Authorization': 'Bearer ' + access_token}

	#upload your video
	retries = 0
	retries_count = 1
	while retries <= retries_count:
	    requset = requests.request('POST', location,headers=headers,data=file_data)
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	# get youtube id
	cont = json.loads(requset.content)            
	youtube_id = cont['id']
	
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 

def Chiama_Url_LiveBroadcast_Control( id, access_token ):
	#https://www.googleapis.com/youtube/v3/liveBroadcasts
	print " ---------------------- INIZIO Chiama_Url_liveBroadcasts ------------------ " 
	url =  'https://www.googleapis.com/youtube/v3/liveBroadcasts/control?part=snippet&id=' + id + '&access_token=' + access_token

	request = urllib2.Request(url)
	print url
	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	print result_dict
	print " ---------------------- FINE Chiama_Url_liveBroadcasts ------------------ " 

def Chiama_Url_Videos( id, access_token ):
	#https://www.googleapis.com/youtube/v3/videos
	print " ---------------------- INIZIO Chiama_Url_Videos ------------------ " 
	url =  'https://www.googleapis.com/youtube/v3/videos?part=snippet,id&mine=true&id=' +id +'&access_token=' + access_token

	request = urllib2.Request(url)
	print url
	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print result_dict
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 


def Chiama_Url_no_params( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json( result_dict )
	if 'stampa' in parse:
		print result_dict


def Chiama_Url_LiveStream( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json_LiveStream( result_dict )
	if 'stampa' in parse:
		print result_dict

def Chiama_Url_LiveBroadcast( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json_LiveBroadcast( result_dict )
	if 'stampa' in parse:
		print result_dict

def Crea_And_Bind_Livestream_Event( nomestream, nomebroadcast,  starttime, privacystatus, access_token ):
	stream_id, ingestion_info = Create_LiveStream( nomestream, 'Test_Description','1080p', 'rtmp','1080p','30fps',  access_token)	
	broadcast_id = Create_LiveBroadcast( nomebroadcast, 'Create_Broadcast_Description', starttime, privacystatus,  access_token)	
	Bind_LiveBroadcast(broadcast_id, stream_id,  access_token)
	return ingestion_info



with open('ingestion_info.json', 'r') as fp:
	ingestion_info = json.load(fp)
print ingestion_info

stream_name =   ingestion_info['streamName']
starttime = ingestion_info['starttime']

List_Applications()

	
