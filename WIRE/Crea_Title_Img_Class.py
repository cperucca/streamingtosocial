import PIL
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from PIL import ImageEnhance
import sys

class Img_Title():

	def __init__(self, title):
		self.width=854
		self.height=480
		#opacity=0.8
		self.opacity=0.0
		self.title= title
		self.filename = 'result.png'
		self.black = (0,0,0)
		self.white = (255,0,0)
		self.transparent = (0,0,0,0)
		self.size = 84

		self.font = ImageFont.truetype("/home/perucccl/.fonts/calibrib.ttf", self.size)

	def Create_Title_Image( self, filename ):

		wm = Image.new('RGBA',(self.width,self.height),self.transparent)
		im = Image.new('RGBA',(self.width,self.height),self.transparent) # Change this line too.

		draw = ImageDraw.Draw(wm)
		w,h = draw.textsize(self.title, self.font)
		draw.text(((self.width-w)/2,(self.height-h)/2),self.title,self.white,self.font)

		en = ImageEnhance.Brightness(wm)

		#en.putalpha(mask)

		mask = en.enhance(1-self.opacity)
		im.paste(wm,(25,25),mask)

		im.save(filename)

		return 'OK'


if __name__ == "__main__":

	if len( sys.argv ) > 1:
		filename = sys.argv[1]
		title = sys.argv[2]
		print ' filename = ' + filename
		print ' title = ' +  title
	else:
		print ' filename = parametro 1 - title = parametro 2 '
		print ' grazie. '
		exit(0)

	imt =  Img_Title( title )
	

	imt.Create_Title_Image( filename )

	print 'salvato img con testo : ' + title + ' e nome : ' + filename

