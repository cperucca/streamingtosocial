import PIL
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from PIL import ImageEnhance
import sys

width=854
height=480
#opacity=0.8
opacity=0.0
title='QUI MANCA IL TESTO'
filename = 'result.png'
black = (0,0,0)
white = (255,0,0)
transparent = (0,0,0,0)
size = 84



font = ImageFont.truetype("/home/perucccl/.fonts/calibrib.ttf", size)

def Create_Title_Image( title, width, height, filename ):

	wm = Image.new('RGBA',(width,height),transparent)
	im = Image.new('RGBA',(width,height),transparent) # Change this line too.

	draw = ImageDraw.Draw(wm)
	w,h = draw.textsize(title, font)
	draw.text(((width-w)/2,(height-h)/2),title,white,font)

	en = ImageEnhance.Brightness(wm)

	#en.putalpha(mask)

	mask = en.enhance(1-opacity)
	im.paste(wm,(25,25),mask)

	im.save(filename)

	return 'OK'


if __name__ == "__main__":

	if len( sys.argv ) > 1:
		title = sys.argv[1]

	Create_Title_Image( title, width, height, filename )
	print 'salvato img con testo : ' + title + ' e nome : ' + filename

