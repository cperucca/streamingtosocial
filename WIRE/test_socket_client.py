#!/usr/bin/env python 
import socket 

# indirizzo IP della Wirecast 2 macchina di test per lo streaming verso social
TCP_IP = '10.72.113.239' 
# porta a caso scelta dal server
TCP_PORT = 6666 
# piu che sufficiente come dimensione del buffer
BUFFER_SIZE = 1024 

def Send_To_Wirecast( message ):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
	s.connect((TCP_IP, TCP_PORT)) 
	s.send(message) 
	data = s.recv(BUFFER_SIZE) 
	s.close() 
	print "received data:", data 
	return data

if __name__ == "__main__":
	
	message = "Start" 
	Send_To_Wirecast( message )
