#!/usr/bin/env python 
import socket 
import wirecast

def Start():
	print 'START'
	return wirecast.Start()
	
def Stop():
	print 'STOP'
	return wirecast.Stop()

def Test():
	print 'Test'
	return wirecast.IsBroadcasting()

def Prendi_Img():
	print 'Prendi Image'
	return 'Tornata Immagine'


options = { 'Test' : Test,
		 'Start' : Start,
            'Stop' : Stop,
		 'Image' : Prendi_Img,
}

def Exec_Action( data ):
	
	if data in options:
		print 'data in options'
		return options[ data ]()
		
	return 'Error'


TCP_IP = '' 
TCP_PORT = 6666 
BUFFER_SIZE = 20 # Normally 1024, but we want fast response 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.bind((TCP_IP, TCP_PORT)) 
s.listen(1) 

while 1: 
	conn, addr = s.accept() 
	print 'Connection address:', addr 
	data = conn.recv(BUFFER_SIZE) 
	if data: 
		#break 
		print "received data:", data 
		data = Exec_Action( data )
	conn.send(data) # echo 
	conn.close() 

