import win32com.client

def Delete_Shot( id ):
	my_Wire = win32com.client.GetActiveObject("Wirecast.Application")
	my_Document = my_Wire.DocumentByIndex(1)

	# lo shot lo aggiungiamo sempre al layer 1
	my_Layer =  my_Document.LayerByIndex(1)
	my_Layer.RemoveShotByID( int(id) )
	return 'Deleted Shot :' + id
	

def Add_Shot( filename ):
	my_Wire = win32com.client.GetActiveObject("Wirecast.Application")
	my_Document = my_Wire.DocumentByIndex(1)

	# lo shot lo aggiungiamo sempre al layer 1
	my_Layer =  my_Document.LayerByIndex(1)
	_shotid = my_Layer.AddShotWithMedia( filename )
	my_Layer.ActiveShotID = _shotid 
	print _shotid
	return _shotid 
	
def IsBroadcasting():
	my_Wire = win32com.client.GetActiveObject("Wirecast.Application")
	my_Document = my_Wire.DocumentByIndex(1)

	if my_Document.IsBroadcasting():
		return 'True is Broadcasting'
	else:
		return 'False is NOT Broadcasting'

def Start():
	my_Wire = win32com.client.GetActiveObject("Wirecast.Application")
	my_Document = my_Wire.DocumentByIndex(1)

	my_Document.Broadcast("start")
	if my_Document.IsBroadcasting():
		return "wirecast Running"
	else:
		return "wirecast Stopped"


def Stop():
	my_Wire = win32com.client.GetActiveObject("Wirecast.Application")
	my_Document = my_Wire.DocumentByIndex(1)

	my_Document.Broadcast("stop")
	if my_Document.IsBroadcasting():
		return "wirecast Running"
	else:
		return "wirecast Stopped"



