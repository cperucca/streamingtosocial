#!/usr/bin/env python 
import socket 
import sys
import Crea_Title_Img


class WireCastClient():
	
	def __init__(self):

		# indirizzo IP della Wirecast 2 macchina di test per lo streaming verso social
		self.TCP_IP = '10.72.113.239' 
		# porta a caso scelta dal server
		self.TCP_PORT = 6666 
		# porta a caso scelta dal server
		self.SERVICE_PORT = 6667 
		# piu che sufficiente come dimensione del buffer
		self.BUFFER_SIZE = 1024 
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

	def Send_To_Wirecast( self, message ):
		# possibili comandi: START STOP TEST IMAGE DEL
		print 'inizio Send_To_Wirecast'
		print message

		self.sock.connect((self.TCP_IP, self.TCP_PORT)) 
		self.sock.send(message) 
		data = self.sock.recv(self.BUFFER_SIZE) 
		self.sock.close() 
		print "received data:", data 
		return data

	def Send_Image_To_Wirecast( filename , titolo):

		print 'inizio Send_Image_To_Wirecast'
		# creo immagine
		Crea_Title_Img.Create_Title_Image( titolo, 1920, 1080, filename )
		

		# uso il socket di cmd per andare il comando immagine
		self.sock.connect((self.TCP_IP, self.TCP_PORT)) 
		self.sock.send('IMAGE ' + filename) 

		print ' cerco di aprire il socket di servizio 6667 '
		s_servizio = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
		connected = False
		count = 0
		# aspetto che il server me lo apra
		while not connected:
		    try:
			s_servizio.connect((self.TCP_IP, self.SERVICE_PORT)) 
			connected = True
			print 'Connesso al socket di servizio'
		    except Exception as e:
			pass #Do nothing, just try again
			count = count + 1
			if count > 10000:
				print 'ERROR missing connection'
				break

		fin = open(filename , 'rb')
		l = fin.read(1024)
		# comincioa a mandare il file
		print 'comincio a mandare il file'
		while (l):
			s_servizio.send(l) 
			l = fin.read(1024)
		s_servizio.close() 
		print ' mandato il file '
		fin.close()

		data = self.sock.recv(self.BUFFER_SIZE) 
		self.sock.close() 
		#print "received data:", data 
		return data

if __name__ == "__main__":

	wcc = WireCastClient()

	if len( sys.argv ) > 1:
		message = sys.argv[1]
	else:
		message = 'Test'
	print wcc.Send_To_Wirecast( message )
