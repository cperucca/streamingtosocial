#!/usr/bin/env python 
import socket 
import sys
import Crea_Title_Img

# indirizzo IP della Wirecast 2 macchina di test per lo streaming verso social
TCP_IP = '10.72.113.239' 
# porta a caso scelta dal server
TCP_PORT = 6666 
# piu che sufficiente come dimensione del buffer
BUFFER_SIZE = 1024 

def Send_To_Wirecast( message ):
	print 'inizio Send_To_Wirecast'
	print message

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
	s.connect((TCP_IP, TCP_PORT)) 
	s.send(message) 
	data = s.recv(BUFFER_SIZE) 
	s.close() 
	#print "received data:", data 
	return data

def Send_Image_To_Wirecast( filename , titolo):

	print 'inizio Send_Image_To_Wirecast'
	# creo immagine
	Crea_Title_Img.Create_Title_Image( titolo, 1920, 1080, filename )
	

	# uso il socket di cmd per andare il comando immagine
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
	s.connect((TCP_IP, TCP_PORT)) 
	s.send('Image ' + filename) 

	print ' cerco di aprire il socket di servizio 6667 '
	s_servizio = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
	connected = False
	count = 0
	# aspetto che il server me lo apra
	while not connected:
	    try:
		s_servizio.connect((TCP_IP, 6667)) 
		connected = True
		print 'Connesso al socket di servizio'
	    except Exception as e:
		pass #Do nothing, just try again
		count = count + 1
		if count > 10000:
			print 'ERROR missing connection'
			break

	fin = open(filename , 'rb')
	l = fin.read(1024)
	# comincioa a mandare il file
	print 'comincio a mandare il file'
	while (l):
		s_servizio.send(l) 
		l = fin.read(1024)
	s_servizio.close() 
	print ' mandato il file '
	fin.close()

	data = s.recv(BUFFER_SIZE) 
	s.close() 
	#print "received data:", data 
	return data

if __name__ == "__main__":

	if len( sys.argv ) > 1:
		message = sys.argv[1]
	else:
		message = 'Test'
	print Send_To_Wirecast( message )
 

