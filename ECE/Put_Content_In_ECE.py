import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil
import glob
import json

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

def Setta_Proxy( url, port, usr, pwd ):

	print urllib2.getproxies()

	proxy = urllib2.ProxyHandler({'http': ''})
	#auth = urllib2.HTTPBasicAuthHandler()
	print proxy
	opener = urllib2.build_opener(proxy, urllib2.HTTPHandler)
	print opener

	urllib2.install_opener(opener)



def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	#Setta_Proxy( '' , '', '', '' )
	print urllib2.getproxies()
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	return


def Dump_Rows ( link, filename ):

	print ' in Dump Rows '

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib2.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			request = urllib2.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib2.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiState( entry , rel):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['name']
		return lis.attrib['name']
	return None



def PrendiEdited_Time( entry , rel):

	list =  entry.findall(namespaces['app'] + "edited/" )
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis.text
		#print lis.attrib['name']
		return lis.text

	return None



def PrendiSezione( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			return lis.attrib['title']
	return None


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			return lis.attrib['href']
	return None


def PrendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return 'None'

	return 'None'

def Prendi_Url( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)
		Url =  PrendiLink( tree, "alternate")
		#print'URL = ' +  Url

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return Url


def Prendi_ProgrammeID( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('117530.xml')
		#ET.dump(tree)
		programmeId =  PrendiField( tree, "programmeId")
		#print'ProgrammID = ' +  programmeId

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return programmeId



def Prendi_Social_Tab( Id_xx ):
		
	lista_value_social = [ 'facebookText', 'facebookAltTitle', 'facebookAltDesc', 'fb-account-rsiNews','fb-rsiNews-sent','fb-account-rsiSport','fb-rsiSport-sent','fb-account-rsiGlobal','fb-rsiGlobal-sent','fb-account-rsiTest','fb-rsiTest-sent','twitterText','tw-account-rsiNews','tw-rsiNews-sent','tw-account-rsiSport','tw-rsiSport-sent','tw-account-rsiTest','tw-rsiTest-sent']

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')
	#link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	link = 'http://internal.publishing2.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	Tab_Json = {}
	try:
	
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('8491108.xml')
		#ET.dump(tree)
		for val in lista_value_social:
			Tab_Json[val] =  PrendiField( tree, val)

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return Tab_Json


def GetSection_and_State( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		#xml = minidom.parse(result)
		#fout = codecs.open("./117530", 'a', 'utf-8')
		#fout.write( xml.toxml() )
		#fout.close()
		
		

		tree = ET.parse(result)
		#tree.write('117530.xml')
		#ET.dump(tree)
		title =  PrendiField( tree, "title")
		print title
		programmeId =  PrendiField( tree, "programmeId")
		print programmeId

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return programmeId

def Update_Sezioni():

	lista_files = glob.glob('./log_to_process')
	#lista_files = glob.glob('./DB/log_2014_11_14_15_02_59*')
	lista_fatti = glob.glob('./tmp/*log')

	#for lis in lista_fatti:
		#os.remove( lis )

	data_old = '2013-12'

	for lis in lista_files:
		print lis
		# apro il file di scrittura che si chiama come il file con sections alla fine
		# e lo metto nella directory SECTIONS
		nomefileout = './tmp/' + data_old + '.log'
		print nomefileout
		
		if nomefileout in lista_fatti:
			continue
		# apro il file di log sello spazio
		fin = codecs.open(lis, 'r', 'utf-8')
		fout = codecs.open(nomefileout, 'w' , 'utf-8')
		# inizializzo il Dict delle sezioni a {} con [ num, size ]
		for line in reversed(fin.readlines()):
			if 'Titolo' in line:
			# per ogni entry del file con Titolo
				# prendo l escenic id escenic:production3034609 con 3034609 = Id_xx
				Id_xx =  line.split('Titolo: escenic:production')[-1].split(' Digital_Item_Id:')[0]
				print Id_xx
				# e ne prendo la size in fondo
				Size_Id_xx = line.split('totale asset Gb = ')[-1]

				# quindi ne prendo la data per sapere dove buttarlo
				data = line.split(' -d- # ')[0].split('-d- ')[-1]
				data = data[0:7]
				print data 
				if data < data_old:
					print 'skip'
					continue
				# e ne vado a prendere l'atom con http://internal.publishing.production.rsi.ch/webservice/escenic/content/Id_xx
				# parso l'Atom risultante 
				# e ne prendo la entry del link section
				[ Sezione_Id_xx, State_Id_xx ] = GetSection_and_State( Id_xx )
				if data == data_old:
					fout.write( line.strip() + '-section-' + Sezione_Id_xx + '-endsection-' + State_Id_xx + '-endstate\n' )
				else:
					print ' devo chiudere il file e aprire il file nuovo '
					fout.close()
					data_old = data
					nomefileout = './tmp/' + data_old + '.log'
					fout = codecs.open(nomefileout, 'w' , 'utf-8')
					fout.write( line.strip() + '-section-' + Sezione_Id_xx + '-endsection-' + State_Id_xx + '-endstate\n' )



		print '\n'
		fout.close()
		fin.close()
		
		# ricomincio con un altro file

def Controlla_ProgrammeID( lista_content ):

	print ' ----------------------- inizia Controlla_ProgrammeID -----------------------'
	print ' lunghezza lista iniziale : ' + str(len(lista_content))
	
	result_list = []

	count = 0
	for lis in lista_content:
		print ' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- '
		count = count + 1
		PGID =   Prendi_ProgrammeID(lis['id'][8:])
		if not (PGID is None) :
			#print PGID
			result_list.append( lis )


	print ' lunghezza lista pulita : ' + str(len(result_list))
	print ' ----------------------- fine Controlla_ProgrammeID -----------------------'
	return result_list
		
	
def Controlla_Social_Tab( lista_content ):

	print ' ----------------------- inizia Controlla_Social_Tab -----------------------'
	print ' lunghezza lista iniziale : ' + str(len(lista_content))
	
	result_list = []

	count = 0
	for lis in lista_content:
		print ' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- '
		count = count + 1
		PGID =   Prendi_Social_Tab(lis['id'][8:])
		if not (PGID is None) :
			#print PGID
			result_list.append( lis )


	print ' lunghezza lista pulita : ' + str(len(result_list))
	print ' ----------------------- fine Controlla_Social_Tab -----------------------'
	return result_list
		
		

if __name__ == "__main__":


	#Dump_Link("http://localhost/webservice/escenic/section/ROOT/subsections", 'jonas_1')
	#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8455456", '8455456.result')
	Dump_Link("http://internal.publishing2.production.rsi.ch/webservice/publication/rsi/escenic/model/shortStory", 'shortmodel.result')
	#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/7963/subsections", 'jonas_tvs')
	#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/8599/subsections", 'jonas_tvsplayer')
	#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/4/subsections", 'jonas_rsi')

	print Prendi_Social_Tab('8491108')

	exit(0)
	print Prendi_Url( 1402222 )


	programmeId  = GetSection_and_State( 117530 )
	print programmeId
	#print 
	#print State_Id_xx
	#print 
	#print Edited_Id_xx
