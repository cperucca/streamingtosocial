
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path
import stat
import Helpers
import shutil
import sys
import time
import re
import subprocess
import json
import urllib
import json
import glob
import Crea_Lista_Content
import Prendi_Content_Da_ECE

def Dump_Link_Vmeo ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('online@vizrt.com', '3sc3niC')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()
	exit(0)
	return

if __name__ == "__main__":

	print ' ----------------- in Procedura_Crea_Lista_Content  ---------------'

	lista_sections = [ '4' ]

	lista_content = []
	contentype = 'shortStory+OR+contenttype%3Astory'

	for lis in lista_sections:
		lista_content =  lista_content + Crea_Lista_Content.Solr_Prendi_Content_From_TVS( lis, contentype )

	print len(lista_content)
	# adesso devo scremare la lista con tutti e SOLO quelli che hanno il programmeID
	#lista_content = Prendi_Content_Da_ECE.Controlla_ProgrammeID( lista_content )

	data_ora = datetime.datetime.now()
	# scrivo in lista_da_cancellare.json la lista di quelli da cancellare
	#fout = codecs.open('./lista_da_valutare.json_'  + data_ora.strftime('%Y_%m_%dT%H_%M_%S'), 'w', 'utf-8')
	#json.dump( lista_content, fout )
	#fout.close()
	
	# e quindi devo andare a fare la mappa con i nuovi match di id aggiornata alla data
	# di oggi al momento dentro la Crea_Map_Ids.py

	#print ' ----------------- in Crea_Map_Ids.py  ---------------'

	#dict = Crea_Map_Ids.Update_DB( ultima_data.strip() )

	#nomefile = 'Map_EscenicId_to_VmeoId_' + data_ora.strftime('%Y_%m_%dT%H_%M_%S')
	#print nomefile
	#print '\n'
	#fout = codecs.open(nomefile, 'w', 'utf-8')
	#json.dump( dict, fout)
	#fout.close()
	
	# adesso devo mettere assieme la ultima mappa con tutte le altre precedentemente create
	#lista_mappe = glob.glob('./Map_EscenicId_to_VmeoId_2016*')
	#map_ece_to_vmeo = {}
	#for lis in lista_mappe:
		#fin = codecs.open( lis, 'r', 'utf-8' )
		#map_ece_to_vmeo.update( json.load( fin ) )
		#fin.close()
		#print lis
	
	#print len(map_ece_to_vmeo)

	'''
	# e aggiorno il Time.log
	#ftime = codecs.open('./Time.log' , 'a', 'utf-8' )
	#ftime.write( data_ora.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z' + '\n')
	#ftime.close()
	
	'''
	# in lista_content ho tutti gli Audio e Video Deleted in rsi
	# nella forma : {'lastmodifieddate': '2013-12-23T01:52:05Z', 'state': 'deleted', 'contenttype': 'migrationAudio', 'id': 'article:60631', 'title': '10.09.13 MILLEVOCI - Burqa e volti coperti.MUS'}

	# mentre in map_ece_to_vmeo ho roba tipo escenic:production2582476 : 154503
	# dove lo id del article deve combaciare la escenic:produciton

	#fin = codecs.open('./lista_da_valutare.json_2016_10_20T15_46_27', 'r', 'utf-8')
	#lista_content = json.load( fin )
	#fin.close()
	#print len(lista_content)

	size = 0
	count = 1
	for lis in lista_content:
		print lis
		ece_id =  lis['id'].split(':')[-1]
		
		url =  Prendi_Content_Da_ECE.Prendi_Url( ece_id ).replace('publishing.tvsvizzera','www.tvsvizzera')
		if not 'publishing.rsi' in url:
			print str(count) + ',' + lis['id'].split(':')[-1] + ',' + url + ',' + str(lis['home_section'] )
			count += 1
		#if map_ece_to_vmeo.has_key(key):
			#print lis['id'], lis['title'], lis['lastmodifieddate'], 	
			#print 'VMEO id = ' + map_ece_to_vmeo[key ]
			#continue
			#tmp_size = Prendi_Size(  map_ece_to_vmeo[ key ] )
			#size  = size + tmp_size
			#print ' Count : ' + str(count)  + ' Tot Size in Mb = ' + str(tmp_size)
			
		#else:
			#print lis['lastmodifieddate'], lis['id'], 'Non trovato'

	#print 'Tot Gb -> ' + str(size/float(1024))
	print ' Presi # ' + str(len(lista_content)) + ' contenuti '

