
# -*- coding: utf-8 -*-

import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import urllib

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'vdf' : '{http://www.vizrt.com/types}',
		'ece' : '{http://www.escenic.com/2007/content-engine}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return


def Dump_Link_Vmeo ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('online@vizrt.com', '3sc3niC')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()
	exit(0)
	return

#print ' Dump_Link_Vmeo '
#link = 'http://10.102.7.21//rest/search/RSI?query=production'
#link = 'http://10.102.7.21//rest/search/RSI?query=36943'
#link = 'http://10.102.7.21//rest/digitalItem/search?provider=RSI&query=title:"escenic:production36943"'
#link = 'http://10.102.7.21//rest/search/RSI?query=production36943'
#link = 'http://10.102.7.21//rest/publishedItems/search/RSI?query=production36943'
#link = 'http://10.102.7.21//rest/digitalItem/status/9255'
#link = 'http://10.102.7.21//rest/digitalItem/status/9255'
#link = 'http://10.102.7.21//rest/digitalItem/status/36943'
#link = 'http://10.102.7.21//rest/publishedItems/search?provider=RSI&query="*production36943"'
#Dump_Link_Vmeo( link, 'test_search')

def Solr_Prendi_Content_From_Rsi( section_id ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:story OR contenttype:programme OR contenttype:shortStory
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true


	# questa con la publishdate
	# link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype&wt=json&indent=true"

	# questa con la lastmodifieddate
	# prende story - programme - shortStory 
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=lastmodifieddate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__SECTION_ID__', section_id).replace('__START__', '0')

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	print response['response']['numFound'], "documents found"

	#print len(response['response']['docs'])

	lista_content_id_data = response['response']['docs']
	print len(lista_content_id_data)
	exit(0)

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #print ' giro sui next e prev '
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			print x+1, __start__ 
			
			#print ' giro per prenderli tutti'
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#print link_next
                        #print link_next
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#print 'Ricevuti : ' + str(len(response['response']['docs']))
			#print  response['response']['docs']

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#print len(lista_content_id_data)


        print ' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data)

        return lista_content_id_data


def Solr_Prendi_Dict_From_Rsi( section_id ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	# prende items con 
	# state:published 
	# creationdate:[NOW-1DAY TO NOW]
	# contenttype:story OR contenttype:programme OR contenttype:shortStory
	# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true


	# questa con la publishdate
	# link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype&wt=json&indent=true"

	# questa con la lastmodifieddate
	# prende story - programme - shortStory 
	# dell'ultima ora
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=lastmodifieddate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
	link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	# CLAD cambiato per avere dei livestreaming
	#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Alivestreaming&fq=lastmodifieddate%3A%5BNOW-1MONTHS+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"

	link = link_template.replace('__SECTION_ID__', section_id).replace('__START__', '0')

	connection = urllib2.urlopen(link)
	response = eval(connection.read() )

	print response['response']['numFound'], "documents found"

	#print len(response['response']['docs'])

	lista_content_id_data = response['response']['docs']
	print len(lista_content_id_data)

        totresult = int(response['response']['numFound'])
        items_per_page = int(100)

        if  totresult > items_per_page:
                #print ' giro sui next e prev '
                # devi  girare sui next per prendere gli altri
                for x in range(int(float(totresult)/float(items_per_page))):
			__start__ = (x+1) * items_per_page
			print x+1, __start__ 
			
			#print ' giro per prenderli tutti'
                        #e qui faccio la request sul campo next
                        link_next = link_template.replace('__SECTION_ID__', section_id).replace('__START__', str(__start__))
			#print link_next
                        #print link_next
                        connection = urllib2.urlopen(link_next)
			response = eval(connection.read() )

			#print 'Ricevuti : ' + str(len(response['response']['docs']))
			#print  response['response']['docs']

			lista_content_id_data = lista_content_id_data +  response['response']['docs']
			
			#lista_content_id_data.append( response['response']['docs'] )
			#print len(lista_content_id_data)


        print ' totale content preso per section ' + section_id + '  = % d ' % len(lista_content_id_data)
	# e adesso lo trasformo in un dizionario
	result_dict = {}
	for lis in lista_content_id_data:
		result_dict[str(lis['id'][8:])] = {"ECEDetails": lis }

	#print result_dict
		

        return result_dict

if __name__ == "__main__":

	def getKey( item ):
		return item[1]
	print ' ----------------- in Crea_List_Audio_Video_RSI ---------------'


	lista_sections = [ '4588' ]

	lista_content = []
	for lis in lista_sections:
		lista_content =  lista_content + Solr_Prendi_Content_From_Rsi( lis )

	print len(lista_content)
	print lista_content
