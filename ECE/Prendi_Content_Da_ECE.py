
# -*- coding: utf-8 -*-

import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import Helpers
import shutil
import glob
import json
from datetime import datetime, timedelta
import pySpin
import ast

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }


for entry in namespaces:
	#print entry, namespaces[entry]
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry


def Dump_ET_Link ( id, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(id)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	tree = ET.parse(result)
	tree.write(filename)

	return



def Dump_Link ( link, filename ):

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)

	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()

	return

def Put_Link( link, filename ):
	
	print '------------------------ inizia Put_Link -------------- '

        base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')
        file = open(filename)
        dati = file.read()

        request = urllib2.Request(link, data=dati)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'PUT'
        result = urllib2.urlopen(request)

        #print result.read()
        return
        xml = minidom.parse(result)
        fout = codecs.open(filename, 'w', 'utf-8')
        fout.write( xml.toxml() )
        fout.close()


        return


def Put_Id( idx, filename ):

	print '------------------------ inizia Put_Id -------------- '

        base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')
        file = open(filename)
        dati = file.read()
	link = "http://internal.publishing.production.rsi.ch/webservice/escenic/content/" + str(idx)

        request = urllib2.Request(link, data=dati)

        request.add_header("Authorization", "Basic %s" % base64string)
        request.add_header('If-Match', '*')
        request.add_header('Content-Type', 'application/atom+xml')
        request.get_method = lambda: 'PUT'
        result = urllib2.urlopen(request)

	print 'Put_Id' 
        print result.read()
	print '------------------------ finisce Put_Id -------------- '
        return



#Dump_Link("http://localhost/webservice/escenic/section/ROOT/subsections", 'jonas_1')

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8709154", '8709154.xml')
#Put_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '../_cambiamento_')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '8568391.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8675248", 'gallery_8675248.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8948557", 'gallery_8948557.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/9013267", '9013267.xml')
#Dump_ET_Link('8845033', '8845033_et.xml')
#exit(0)

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/7963/subsections", 'jonas_tvs')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/8599/subsections", 'jonas_tvsplayer')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/4/subsections", 'jonas_rsi')


def Dump_Rows ( link, filename ):

	print ' in Dump Rows '

	#base64string = base64.encodestring('%s:%s' % ('perucccl', 'Mediaclad45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib2.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	print '  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page)

	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib2.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	if  totresult > items_per_page:
		#print ' giro sui next e prev '
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#print link_next
			request = urllib2.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib2.urlopen(request)
			tree = ET.parse( result )
			request = urllib2.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib2.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiState( entry , rel):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	print list

	for idx , lis in enumerate(list):
		print idx, lis
		print lis.attrib['name']
		return lis.attrib['name']
	return None

def PrendiEdited_Time( entry , rel):

	list =  entry.findall(namespaces['app'] + "edited/" )
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis.text
		#print lis.attrib['name']
		return lis.text

	return None



def PrendiSezione( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			return lis.attrib['title']
	return None

def PrendiTipo( entry ):

        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                #print lis
                payload =  lis.findall(namespaces['vdf'] + "payload")
		tipo = payload[0].attrib['model'].split('/')[-1]
		return tipo

	return None


def PrendiPayload( entry , rel):

	list =  entry.findall(namespaces['atom'] + "payload")
	
	print len(list)

	for idx , lis in enumerate(list):
		print idx, lis
		print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			print lis.attrib['href']
			print lis.attrib['title']
			#return lis.attrib['href']
	return None


def PrendiLinkRelated( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print len(list)

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			return lis
			
	return None


def PrendiLinkRelatedId( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print len(list)

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			return lis.attrib['href'].split('/')[-1]
			
	return None

def Trova_KeyFrame( Id_xx ):


	print ' ------------------- INIZIO Trova_KeyFrame ------------------------- '
	
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)

		id_keyframe =  PrendiLinkKeyframe( tree , 'related')




	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None']

	print ' ------------------- FINE Trova_KeyFrame ------------------------- '
	return id_keyframe

def PrendiLinkKeyframe( entry , rel):
	

        list =  entry.findall(namespaces['atom'] + "link")

        #print len(list)
	id_keyframe = ''

        for idx , lis in enumerate(list):
                #print idx, lis
                #print lis.attrib['rel']
                if rel in lis.attrib['rel']:
                        #for idx2, attr in enumerate(lis.attrib):
                                #print idx2, attr
                        print lis.attrib[namespaces['metadata'] + 'group']
                        group =  lis.attrib[namespaces['metadata'] + 'group']
			if 'KEYFRAMES' in group:
				# ho trovato un keyframe
				_payload = lis.findall( namespaces['vdf'] + "payload")
				print _payload[0].attrib['model']
				if 'EDITORIAL' in group:
				
					print lis.attrib['href']
					print lis.attrib['title']
					#  se editorial ritorno con ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# se normale me lo ricordo ma cerco editorial
					id_keyframe = lis.attrib['href'].split('/')[-1] 

        return id_keyframe


def PrendiLinkLeadId( entry , rel):
	
	# al momento prende sempre e solo la prima
	# e assume che sia una immagine

        list =  entry.findall(namespaces['atom'] + "link")

        #print len(list)

        for idx , lis in enumerate(list):
                #print idx, lis
                #print lis.attrib['rel']
                if rel in lis.attrib['rel']:
                        #for idx2, attr in enumerate(lis.attrib):
                                #print idx2, attr
                        #print lis.attrib[namespaces['metadata'] + 'group']
                        group =  lis.attrib[namespaces['metadata'] + 'group']
			if 'lead' in group:
				# ho trovato una relazione in lead
				# devo verificare che sia una picture
				_payload = lis.findall( namespaces['vdf'] + "payload")
				#print _payload[0].attrib['model']
				if 'picture' in _payload[0].attrib['model']:
				
					#print lis.attrib['href']
					#print lis.attrib['title']
					# e qui ne prendo ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# devo prendere la immagine del video
					# altrimenti il video stesso
					print 'Trovato VIDEO ? '
					# devo prendere l id della picture che mi interessa
					# poi dovro prendere il valore del video stesso e buttarlo
					# su
					id_keyframe = Trova_KeyFrame( lis.attrib['href'].split('/')[-1] )
					return id_keyframe

        return None


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#print len(list)

	for idx , lis in enumerate(list):
		#print idx, lis
		#print lis.attrib['rel']
		if rel in lis.attrib['rel']:
			#print lis.attrib['href']
			#print lis.attrib['title']
			
			return lis.attrib['href']
	return None

def PrendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None

def PrendiFieldSocial( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None




def processElem(elem):
    if elem.text is not None:
        print elem.text
    for child in elem:
        processElem(child)
        if child.tail is not None:
	    print ' scrivo la coda ' 
            print child.tail


def PrendiFieldTest( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				print idx, fiel
				print fiel.attrib['name']
				if rel in fiel.attrib['name']:
					print ' trovato ' + rel 
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						val =  fiel.find(namespaces['vdf'] + "value")
						processElem( val )
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return 'None'

	return 'None'



def PrendiTimeCtrl( entry ):

	
	activation = PrendiActivation(entry, ' activation_time')
	#print 'activation ' + activation
	published = PrendiPublished( entry, 'updated')
	#print 'published ' + published
	expires = PrendiExpires( entry, 'expiration' )
	#print 'expires ' + expires

	return [ activation, published, expires ]

def PrendiPublished( entry , name):

	list =  entry.findall(namespaces['atom'] + name )
	for lis in list:
		#print lis.text
		return lis.text

	return None




def PrendiExpires( entry , rel):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		#print lis.text
		return lis.text

	return None



def PrendiActivation( entry , rel):

	list =  entry.findall(namespaces['dcterms'] + "available")
	for lis in list:
		#print lis.text
		return lis.text

	return None


def CambiaBodyFile( filein ):

	replacements = {'<html:':'<', '</html:':'</','xmlns:html=':'xmlns=' }

	lines = []
	with open(filein) as infile:
	    for line in infile:
		for src, target in replacements.iteritems():
		    line = line.replace(src, target)
		lines.append(line)
	infile.close()
	with open(filein, 'w') as outfile:
	    for line in lines:
		outfile.write(line)	
	outfile.close()
	

def CambiaState( entry , value):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	#print ' CambiaState ------ '
	#print list

	for idx , lis in enumerate(list):
		#print idx, lis
		lis.text = value
		return entry

	#print ' CambiaState ------ '
	return None



def CambiaField( entry , rel, value):

        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #print fields

                        for idx , fiel in enumerate(fields):
                                #print idx, fiel
                                #print fiel.attrib['name']
                                if rel in fiel.attrib['name']:
                                        if fiel.find(namespaces['vdf'] + "value") is None:
                                                fiel.append(ET.Element(namespaces['vdf'] + "value"))
                                        fiel.find(namespaces['vdf'] + "value").text = value
					#print fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text
                                        return entry

        return entry

def Change_Item_Content( title, description, alttext, leadtext, tree ):

        if leadtext is not None:
                # allora devo riempirlo con il leadtext
                print "leadtext is not None = " + leadtext
                CambiaField( tree, "alttext", leadtext )
                return tree
        #print "alttext vuoto"
        else:
                if description is not None:
                        # allora devo riempirlo con la descrizione
                        print "description is not None = " + description
                        CambiaField( tree, "alttext", description )
                        return tree
                else:
                        # allora devo riempirlo con il titolo
                        print "riempio con il titolo = " + title
                        CambiaField( tree, "alttext", title )
                        return tree


        return tree


def Get_Item_Content( Id_xx ):

        base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

        link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

        request = urllib2.Request(link)

        request.add_header("Authorization", "Basic %s" % base64string)
        try:
                result = urllib2.urlopen(request)
                tree = ET.parse(result)
                #tree.write('x.xml')
                #ET.dump(tree)

                tipo = PrendiTipo( tree )

		print tipo

		return


                if 'keyframe' in tipo:
                        #se e' un keyframe si chiama name
                        campo_titolo = "name"
                else:
                        #altrimenti si chiama title
                        campo_titolo = "title"
                title =  PrendiField( tree, campo_titolo)
                print title
                description =  PrendiField( tree, "description")
                print description
                alttext =  PrendiField( tree, "alttext")
                print alttext
                leadtext =  PrendiField( tree, "leadtext")
                print leadtext
                #programmeId =  PrendiField( tree, "programmeId")
                #print programmeId

                result.close()
        except urllib2.HTTPError, e:
                print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
                return [ u'None' , u'None', u'None']


        return [ title , description, alttext, leadtext, tree ]


def Prendi_Url( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)
		Url =  PrendiLink( tree, "alternate")
		#print'URL = ' +  Url

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return Url

def Check_Data_Range( adesso, Id_xx ):

	# questa server per verificare di essere nel range tra activation
	# e expiration 
	# verifico anche la publishing perche talvolta e piu alta della activation date

	[ activation, published, expires ] =  Prendi_Social_TimeCtrl(Id_xx)
	# print activation, published, expires

	data_limite = adesso
	active = False

	# print ' adesso = ' + str(adesso)

	# in caso non sia settata alcuna data di attivazione la Prendi_Social_TimeCtrl
	# mi torna None per il campo activate 
	if activation is None:
		# mettendo il seguente aassegnamento sono sicuro poi di confrontare
		# la data attuale con la data  di pubblicazione e dovrei passare sempre
		active = True
	else :
		data_limite = datetime.strptime( activation, '%Y-%m-%dT%X.000Z' )

	# print 'data_limite = ' + str(data_limite)
	
	if adesso >= data_limite:	
		# print ' Data attuale nel Range '
		return True
	
	# print ' Data attuale FUORI Range '
	return False

	



def Prendi_Social_TimeCtrl( Id_xx ):
	
	flagDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		timeCtrl = PrendiTimeCtrl( tree )

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ None , None]
		
	
	return timeCtrl

def VerificaExtraStream( entry ):

        result = []
        list =  entry.findall(namespaces['atom'] + "content")
        for lis in list:
                #print lis
                payload =  lis.findall(namespaces['vdf'] + "payload")
                for pay in payload:
                        fields = pay.findall(namespaces['vdf'] + "field")
                        #print fields

                        for idx , fiel in enumerate(fields):
                                #print idx, fiel
                                #print fiel.attrib['name']
                                if 'extraStream' == fiel.attrib['name']:
                                        #print 'passo di qui'
                                        # qui ho preso il field fb-livestreaming-account
                                        # e adesso devo prendere tutti gli account che ho messo
                                        _list = fiel.findall(namespaces['vdf'] + "list")
                                        if len(_list) > 0:
                                                _list = _list[0]
                                        else:
                                                return False
                                        stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
                                        #print 'stre_payloads '
                                        #print stre_payloads
                                        for stre in stre_payloads:
                                                #print 'giro _payloads'
                                                #print stre
                                                stre_field = stre.find(namespaces['vdf'] + "field")
                                                #print stre_field.attrib['name']
                                                if 'extraStream' in stre_field.attrib['name']:
                                                        if not (stre_field.find(namespaces['vdf'] + "value") is None) :
                                                                #print stre_field.find(namespaces['vdf'] + "value").text
                                                                if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
                                                                        return True
                                                                #result.append(acc_field.find(namespaces['vdf'] + "value").text)

        return False



def VerificaGeo( entry ):

	result = []
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if 'stream' == fiel.attrib['name']:
					#print 'passo di qui'
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) > 0 :
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#print 'stre_payloads ' 
					#print stre_payloads
					for stre in stre_payloads:
						#print 'giro _payloads'
						#print stre
						stre_field = stre.find(namespaces['vdf'] + "field")
						#print stre_field.attrib['name']
						if 'stream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#print stre_field.find(namespaces['vdf'] + "value").text
								if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
									return True
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)	

	return False

def PrendiAccounts( entry , type ):

	result = []
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if type in fiel.attrib['name']:
					#print 'passo di qui'
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) > 0 :
						_list = _list[0]
					else:
						return result
					acc_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#print 'acc_payloads ' 
					#print acc_payloads
					for acc in acc_payloads:
						#print 'giro _payloads'
						#print acc
						acc_field = acc.find(namespaces['vdf'] + "field")
						#print acc_field.attrib['name']
						if type in acc_field.attrib['name']:
							if not (acc_field.find(namespaces['vdf'] + "value") is None) :
								#print acc_field.find(namespaces['vdf'] + "value").text
								result.append(acc_field.find(namespaces['vdf'] + "value").text)	

	return result


def PrendiTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print lis
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print fields

			for idx , fiel in enumerate(fields):
				#print idx, fiel
				#print fiel.attrib['name']
				if 'startTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
				if 'endTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text

	return [ start, end ]





def SistemaYtAccounts( SocialFlags ):

	result = []
	#da ytl-account-rsi
	#devono diventare una lista cosi ['testare_e_bello', 'linea_rossa', 'patti_chiari', 'rsi_sport', 'rsi_news'] 
	filtered_dict = dict((key,value) for key, value in SocialFlags.iteritems() if 'account' in key )
	for key, value in filtered_dict.iteritems(): 
		if value is not None:
			if 'true' in value:
				# e qui prendo solo la parte descrittiva della chiave dell account
				result.append(key.split('account-')[-1])


	return result

def SistemaTime( Item_Json ):

	print " ---------------------- INIZIO SistemaTempo   ------------------ "


	# prende il piu' basso tra i tempi di inizio e terminazione
	# prendendo in considerazione activation e starttime per inizio
	# e expiratione e endTime per la fine

	# dato che poi FB vuole il tempo timezonato aggiundo la timedifference
	delta = datetime.now() - datetime.utcnow()

	start = Item_Json['startTime']
	end = Item_Json['endTime']


	print ' sistema tempo : start, end '
	print start, end

	if (not (Item_Json['activation'] is None) ) :
		activa = datetime.strptime( Item_Json['activation'], '%Y-%m-%dT%X.000Z' )
		_tmp_start = datetime.strptime( Item_Json['startTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if activa > _tmp_start:
			start = datetime.strftime( Item_Json['activation'], '%Y-%m-%dT%H:%M:%SZ' )

	
	if (not (Item_Json['expiration'] is None) ) :
		expira = datetime.strptime( Item_Json['expiration'], '%Y-%m-%dT%X.000Z' )
		_tmp_end = datetime.strptime( Item_Json['endTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if  expira < _tmp_end:
			end = datetime.strftime( Item_Json['expiration'], '%Y-%m-%dT%H:%M:%SZ' )

	print " ---------------------- END SistemaTempo   ------------------ "


	start = datetime.strptime( start,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	start = datetime.strftime( start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime( end,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	end = datetime.strftime( end, '%Y-%m-%dT%H:%M:%SZ')

	print ' sistema tempo : start, end '
	print start, end
	# ritorno nella forma
	# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
	
	return [ start, end ]


def SistemaTitolo( Tab_Json ):
	
	if ( Tab_Json['fbVideoTitle'] is None ) or ( len(Tab_Json['fbVideoTitle']) < 1 ):
		Tab_Json['fbVideoTitle'] = Tab_Json['title']

	if ( Tab_Json['fbVideoDesc'] is None ) or ( len(Tab_Json['fbVideoDesc']) < 1 ):
		if  not ( Tab_Json['title'] is None ) :
			Tab_Json['fbVideoDesc'] = Tab_Json['subtitle']
		else:
			Tab_Json['fbVideoDesc'] = ''

	return Tab_Json

def Prendi_Flag_Social( Id_xx ):
	
	lista_value_social = [ 'title','subtitle', 'fbVideoTitle', 'fbVideoTags', 'fbVideoDesc', 'ytVideoTitle','ytVideoDesc','ytVideoTags','yt-account-rsi','yt-account-spam','yt-account-test', 'fbSenzaNotifica']

	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')
	#link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)
	request = urllib2.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	Tab_Json = {}
	try:
	
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('8491108.xml')
		#ET.dump(tree)
		for val in lista_value_social:
			Tab_Json[val] =  PrendiField( tree, val)

		Tab_Json = SistemaTitolo( Tab_Json )

		# qui sistemo gli account di YT
		Tab_Json['yt-livestreaming-account'] = SistemaYtAccounts( Tab_Json )

		# e adesso prendo gli account fi facebook a cui spedirlo
		Tab_Json['fb-livestreaming-account'] = PrendiAccounts( tree , 'fb-livestreaming-account')

		# e adesso prendo lo start e end time
		[ Tab_Json['startTime'], Tab_Json['endTime'] ] = PrendiTime( tree )

		# e adesso prendo i check times = activation expire ...
		[ Tab_Json['activation'], Tab_Json['published'], Tab_Json['expiration'] ] = PrendiTimeCtrl( tree )

		# sistemo i tempi per avere t_start e t_end
		[ Tab_Json['t_start'], Tab_Json['t_end'] ] = SistemaTime( Tab_Json )


		# e adesso prendo le info di Geoblocking
		Tab_Json['Geo'] = VerificaGeo( tree )
		if not (Tab_Json['Geo']):
			Tab_Json['Geo'] = VerificaExtraStream( tree )

		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	return Tab_Json






def Prendi_Altro_Content( Id_xx ):

	print ' INIT Prendi_Altro_Content '
	
	contentDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#print ET._namespace_map
		#tree.write('rimpingua_content_con_img.xml')
		#ET.dump(tree)
		#print key

		# come fatto qui sotto posso prendere tutti i fields
		title =  PrendiField( tree, 'title' )
		subtitle = PrendiField( tree, 'subtitle' )
		leadtext = PrendiField( tree, 'leadtext' )
		print title, subtitle , leadtext
		# e title e subtitle li metto direttamente al posto giusto

		#body =   PrendiFieldTest( tree, 'body')
		#print body
		#print ' fine body '


		#print PrendiField( tree, 'leadtext' )
		#print PrendiLinkRelatedId( tree, 'related')
		#print PrendiLinkLeadId( tree, 'related')
		
		tipo = PrendiTipo( tree )
		if 'short' in tipo:
			link_alla_url = 'http://www.rsi.ch/web/ultimora/?a=' + Id_xx
		else:
			#link_alla_url = Prendi_Url( Id_xx )
			link_alla_url = 'http://www.rsi.ch/g/' + Id_xx

		link_alla_url = link_alla_url.replace('publishing','www')

		rel_id = PrendiLinkLeadId( tree, 'related')
		if not( rel_id is None):

	
			'''

			# eventualmente per prendere la caption
			related_content = PrendiLinkRelated( tree, 'related')

			lista =  related_content.findall(namespaces['vdf'] + "payload")
			print len(lista)
			for lis in lista:
				print lis.attrib
			print related_content
			exit(0)

			''' 
			caption = ''




			url = 'https://www.rsi.ch/rsi-api/resize/image/BASE_FREE/' + rel_id
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':caption}
		else:
			url = None
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':''}
		
		result.close()
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None']
		
	
	print contentDiRitorno
	return contentDiRitorno 

def Cambia_Flag_Social( Id_xx , UpdateFlags ):
	
		
	print ' --------------------------- init Cambia_Flag_Social ----------------------'
	base64string = base64.encodestring('%s:%s' % ('perucccl', 'perucccl')).replace('\n', '')

	link = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/' + str(Id_xx)

	request = urllib2.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib2.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('8568391.xml')
		# esempio di modifica del file xml 
		#tmp_tree =  CambiaField( tree, "facebookText", 'CAMBIATOOOO' )
		#tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('8568391_cambiato.xml')
		#exit(0)
		
		#ET.dump(tree)
		for key, value in UpdateFlags.iteritems():

			# fix per non modificare i campi data
			# perche se non ha alcun valore io ci sbatterei dentro None 
			# al posto di niente
			if 'Time' in key:
				continue
			# invece cosi non li cambio per niente e mi ritrovo
			# magicamente i valori che erano settati prima.
			

			print key, value
			if value is None:
				value = 'None'
			else:
				if  isinstance( value, ( int ) ):
					print value
					value = str( value )
				
			if 'None' in value:
				continue
				

			# come fatto qui sotto posso prendere tutti i fields
			tmp_tree =  CambiaField( tree, key, value )
			tree = tmp_tree

		print 'Dump CLAD'
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		print ' --------------------------- fine Cambia_Flag_Social ----------------------'
		return tree
	except urllib2.HTTPError, e:
		print ' EXCEPTIOOOONNNNNN - ritorno none !!!! '
		return [ u'None' , u'None', u'None']
		
	
	print ' --------------------------- fine Cambia_Flag_Social ----------------------'
	return flagDiRitorno


def Rimpingua_Content( lista_content ):

	print ' ----------------------- inizia Rimpingua_Content -----------------------'
	
	result_list = []

	count = 0
	for lis in lista_content:
		#print ' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- '
		count = count + 1
		lis['lead'] = Prendi_Altro_Content(lis['id'][8:])
		lis['link'] = 'www.rsi.ch/g/'+   lis['id'][8:]
		print lis['lead']
		# prima la lista da_fare la aggiungevo qui .....
		# adesso per motivi di performance  ho spostato sotto il rimpingua content
		# che faccio solo su quelle che devo postare 
		# e la lista da_fare la aggiungo prima
		#lis['da_fare'] = []
		result_list.append( lis )
		
	return result_list

	print ' ----------------------- fine Rimpingua_Content -----------------------'

def Aggiungi_Items_al_Db( archive_name, archive_dict ):
	
	if len(archive_dict) == 0 :
		return
        old_dict = {}
	if os.path.isfile( archive_name ):
		adesso = datetime.utcnow()
		shutil.copy( archive_name, archive_name + "_" + adesso.strftime("%s") )
		try :
			print 'AGGIUNGO a  ARCHIVE_NAME ' + archive_name
			fin = codecs.open( archive_name, 'r', 'utf-8' )
			old_dict = json.load( fin )
			print old_dict
			fin.close()
		except:
			print 'PROBLEMI CON IL DB'
			pass


	for key,value in archive_dict.iteritems():
		old_dict[ key ] = value


        fout = codecs.open( archive_name, 'w', 'utf-8')
        json.dump( old_dict, fout )
        fout.close()



def Scrivi_Items_Db( db_name, lista ):

	
	
	#if len(lista) == 0 :
		#return

	#adesso = datetime.utcnow()

	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )

        fout = codecs.open( db_name, 'w', 'utf-8')
        json.dump( lista, fout )
        fout.close()

def Prendi_Items_Db( db_name ):

        result_dict = {}
	try :
		#print 'leggo DB_NAME ' + db_name
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_dict = json.load( fin )
		#print result_dict
		fin.close()
	except:
		print 'PROBLEMI CON IL DB'
		pass

        return result_dict

def Aggiungi_Items_da_Db( db_name, dict_content):

	# deve aggiungere tutti e solo gli item che ci non nel Db e che mancano
	# alla query del Solr cioe non sono stati modificati nell'ultimo minuto

	#print dict_content
	result = dict_content

	for key,value in dict_content.iteritems():
		dict_content[ key ]['FBStreamingDetails'] = {}
		dict_content[ key ]['YTStreamingDetails'] = {}
	
	# quindi prendo quelli del db
	dict_db = Prendi_Items_Db( db_name )
	print '  totale content preso dal DB        =  ' + str(len(dict_db))
	# e verifico che non siano gia' presenti nella dict_content
	for key, value in dict_db.iteritems():
		#print key
		#print value
		#print 
		#print value['SocialFlags']
		#print value['ECEDetails']
		
		_appendo = True
		if key in dict_content:
			# questo non lo aggiungo
			_appendo = False
			# se esistono gia i video details allora li copia nelle nuove info che arrivano da ECE
			# magari qualcuno ha cambiato qualche valore di time e dobbiamo riverificare tutto
			# in base a quell id ma non perdere i videodetail e neppure i wowza details
			if 'FBStreamingDetails' in value:
				dict_content[ key ]['FBStreamingDetails'] = value['FBStreamingDetails']
			if 'YTStreamingDetails' in value:
				dict_content[ key ]['YTStreamingDetails'] = value['YTStreamingDetails']
					
		if _appendo:
			result[ key ] = value
	
	return result

def printList( dict_content ):


	for key, value in dict_content.iteritems():
		print ' Key -----> ' + key
		if 'ECEDetails' in value :
			print '----  ECEDetails : '	
			print value['ECEDetails']
		if 'SocialFlags' in value :
			print '----  SocialFlags : '	
			print value['SocialFlags']
		if 'FBStreamingDetails' in value and len(value['FBStreamingDetails']) > 0:
			print '----  FBStreamingDetails : '	
			for keystream,valuestrem in value['FBStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		if 'YTStreamingDetails' in value and len(value['YTStreamingDetails']) > 0:
			print '----  YTStreamingDetails : '	
			for keystream,valuestrem in value['YTStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		print '------------------------- fine ' + key + '-----------------------'
	

def Controlla_Flag_Social( lista_content ):

	print '----------------------- inizia Controlla_Flag_Social -----------------------'
	#print ' lunghezza lista iniziale : ' + str(len(lista_content))
	#print lista_content
	#print ' da fare : '


	# questa deve preparare la lista di quelle da fare adesso e quelle da scrivere nel DB
	# e quelli da fare sono quelli da aprire o chiudere
	# quindi che hanno appena passato lo starttime
	# o che hanno appena passato endTime
	
	da_fare_list_ON = {}
	da_fare_list_OFF = {}
	da_fare_list_PREPARA = {}

	
	# per controllo su data emissione mi server il tempo attuale
	print 'prendo tempo attuale in utc '
	# cosi considero anche eventuali cambiamenti di daysaving time 
	adesso = datetime.now()
	print adesso

	count = 0
        #spinner = pySpin.Spinner()
        #spinner.start()
        # ... some long-running operations
        # time.sleep(3)
	for key,value in lista_content.iteritems():
		# giro su tutti gli itemspresi sia da Solr che dal mio db
		print ' - ' + str(count) + ' -- ' + key + ' --- '
		count = count + 1

		# qui prendo le social flags
		# se arrivo dal db le riprendo cmq nel caso qualcuno avesse cambiato qualcosa nell'ultimo
		# minuto 
		value['SocialFlags'] =   Prendi_Flag_Social( key )

		print 'debug delle social flags'
		#print 'SocialFlags'
		#for k,v in value['SocialFlags'].iteritems():
			#print k,v


		# qui metto il controllo che ci sia almeno un account
		# a cui spedirlo altrimenti salto
		if len(value['SocialFlags']['fb-livestreaming-account'] ) < 1 and len(value['SocialFlags']['yt-livestreaming-account'] ) < 1:
			print ' ESCO perche non ci sono le FlagSocial settate'
			continue
	
		# i casi sono tre
		if adesso < datetime.strptime( value['SocialFlags'] ['t_start'],  '%Y-%m-%dT%H:%M:%SZ' ):
			# devo preparare il video e in caso ci sia notifica mandarla
			da_fare_list_PREPARA[ key ] = value
			# la metto da_scrivere
			# ci sara' da fare qualcosa 
		else:
			if adesso > datetime.strptime( value['SocialFlags'] ['t_end'],  '%Y-%m-%dT%H:%M:%SZ' ):
				# devo spegnere
				da_fare_list_OFF[ key ] = value
			else:
				if len(value['FBStreamingDetails']) > 0:
					if 'VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
						if value['FBStreamingDetails']['VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0]]['status'] == 'LIVE_STOPPED':
							continue
				else:
					print 'QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n'

				if len(value['YTStreamingDetails']) > 0:
					if 'VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0] in value['YTStreamingDetails']:
						if value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete':
							continue
				else:
					print 'QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n'


				# devo accendere oppure e gia accesa
				da_fare_list_ON[ key ] = value
				# e cmq ci sara qualcosa da fare

		

        #spinner.stop()
	print ' lunghezza lista PREPARA  : ' + str(len(da_fare_list_PREPARA))
	printList (da_fare_list_PREPARA )
	print ' lunghezza lista ON  : ' + str(len(da_fare_list_ON))
	printList ( da_fare_list_ON )
	print ' lunghezza lista OFF  : ' + str(len(da_fare_list_OFF))
	printList ( da_fare_list_OFF )
	print ' ----------------------- fine Controlla_Flag_Social -----------------------'
	return [ da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF ]
		

		
def Update_Flag_Social( lista_content ):

	print ' ----------------------- inizia Update_Flag_Social -----------------------'
	print ' lunghezza lista iniziale : ' + str(len(lista_content))
	
	result_list = []

	count = 0
	for lis in lista_content:
		print ' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- '
		count = count + 1
		print lis['social_flags']

		# il valore di ritorno del forceResend DEVE SEMPRE essere a false
		lis['social_flags']['tw-forceResend'] = 'false'
		lis['social_flags']['fb-forceResend'] = 'false'

	
		flagsdiritorno =  Cambia_Flag_Social(lis['id'][8:], lis['social_flags'])

		# CLADDDDDD #

		print ' tornato in Update_Flag da Cambia _Flag_Social '
		print PrendiState( flagsdiritorno, 'dummy' )
		flagsdiritorno =  CambiaState(flagsdiritorno, "published")

		try:
			os.remove('/home/perucccl/PushToSocial/_cambiamento_')
			print ' remove finto'
		except:
			pass

		flagsdiritorno.write('/home/perucccl/PushToSocial/_cambiamento_')
		# brutto fix : cambiamo <html: e </html: direttamente nel file
		CambiaBodyFile( '/home/perucccl/PushToSocial/_cambiamento_')

		Put_Id( lis['id'][8:], '/home/perucccl/PushToSocial/_cambiamento_')

	print ' ----------------------- fine Update CLAD Flag_Social -----------------------'
	return result_list
		
		

if __name__ == "__main__":

	#Get_Item_Content('8845033')
	#exit(0)

	#Put_Id( 8732965, './_test_cambiamento_')
	#exit(0)

	#print Prendi_Social_TimeCtrl('8730301')
	# print Prendi_Social_TimeCtrl('8568391')
	
	#print Check_Data_Range( datetime.utcnow(), '8568391')
	#print 'Adesso CLAD'
	#print Prendi_Altro_Content( '8730301')
	#exit(0)

	#lista =  Prendi_Flag_Social( 8730301 )
	#for lis in lista.iteritems():
		#print lis
	#print lista
	#exit(0)
	lista_da_passare = { 'id':'1234567:8568391'}
	lista_da_passare = {'contenttype': 'story', 'lead': {'picture': None, 'caption': '', 'link': 'http://www.rsi.ch/temp/test-push-to-social-Civi-8568391.html', 'name': 'test push to social Civi', 'description': 'subhead'}, 'title': 'test story title', 'lastmodifieddate': '2017-01-10T13:23:23Z', 'state': 'published', 'da_fare': [], 'id': 'article:8568391'}

	Controlla_Flag_Social( [ lista_da_passare])
	#print 'in main'
	#Prendi_Altro_Content( '8568391')
	exit(0)
	#programmeId  = GetSection_and_State( 117530 )
	#print programmeId
	#print 
	#print State_Id_xx
	#print
	#print Edited_Id_xx
