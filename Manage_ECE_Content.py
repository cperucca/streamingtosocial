
# -*- coding: utf-8 -*-


import ast
import os
import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import json
from datetime import datetime, timedelta
import time

import ECE.Crea_List_Livestreaming as Crea_List_Livestreaming
import ECE.Prendi_Content_Da_ECE as Prendi_Content_Da_ECE
import FB.pyFacebookClass as pyFacebookClass
import YT.pyYoutubeClass as pyYoutubeClass
import WOW.pyWowzaClass as pyWowzaClass
import Keys.pySettaVars as pySettaVars
import APICoreX.API_DB as API_DB

def Croppa_Titolo( titolo, link ):

	print len(titolo)
	
	
	delta = len( link) + 5
	# il delta e +5 se con i puntini mentre e 
	# +1 se senza puntini

	
	if len(titolo) + delta > 138:
		print ' > 138'
 
		puntini = u'...'
	else:
		print ' < 138'
		puntini = u''

	while len( titolo ) + delta > 138:
		#print titolo.split(' ')
		#print titolo.split(' ')[-1]
		lunghezza = -(len(titolo.split(' ')[-1]) + 1)
		#print lunghezza
		titolo = titolo[0:lunghezza]
		#print len(titolo)
		#print titolo

	#print titolo + '...'
	titolo = titolo + puntini

	#print titolo + u' ' + link

	return titolo + u' ' + link

def Croppa_Coda_Testo( testo ):

	_debugga_ = True

	if _debugga_ :
		print len(testo)

	while len( testo ) > 138:
		#print testo.split(' ')
		#print testo.split(' ')[-1]
		lunghezza = -(len(testo.split(' ')[-1]) + 1)

		#print lunghezza
		testo = testo[0:lunghezza]
		if _debugga_ :
			print len(testo)
			print testo

	if _debugga_ :
		print testo + '...'


	return testo + '...'




def Componi_Testo_Limite( name, link, desc ):
	# questa limita a 140 caratteri quello che vogliamo buttare su
	totale = name + u' ' + link + u' ' + desc 	
	print totale

	if len(totale) > 138:
		if len(name + u' ' + link) < 138:
			#print 'titolo troppo lungo ma croppo solo in fondo'
			totale = Croppa_Coda_Testo( totale )
		else:
			# vuol dire che devo togliere dal titolo il numerodi caratteri del link 
			# piu tre puntini che aggiungero in fondo
			#print 'devo cavare dal titolo link e tre puntini'
			totale = Croppa_Titolo( name, link )
			
	print 'result = ' + totale
	return totale


#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: n insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTTitle: test 4.0 post su social',u'http://www.rsi.ch/g/8845033' ,  u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di')
#Componi_Testo_Limite(u'TESTSubTitle: test 4.0 post su social ata su più fronti. Tra il 2008 e il 2010 cè stato un progressivo aumento del numero di zanzariere trattate con insetticidi e distribuite a oltre 578 milioni di', u'http://www.rsi.ch/g/8845033' , u'TESTTitle: test 4.0 post su social')
#exit(0)


def ChooseAccount( da_fare_under, lis ):

	print ' ------------- INIT ChooseAccount --------------------------------------- '
	abs_account = None
	msg = u''
	fb_title = ''
	fb_alt_desc = ''

	if 'fb' in da_fare:
		try :
			print 'in FB'
			abs_account = pyFacebookClass.FacebookConnection( da_fare_under )

			# print abs_account
			# e devo anche mettere il testo giusto
			# per facebook ci potrebbero anche stare 
			#'link':'http://www.rsi.ch', oppure all articolo
			#'picture':'http://pngimg.com/upload/water_PNG3290.png',
			#'name':'NAME WATER',
			#'description':'DESCRIPTION WATER',
			#'caption':'CAPTION WATER'
			# da vedere

			if lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] :
				print ' 1 ' 
			else:
				print ' 2 '

			if not (lis['social_flags']['facebookAltTitle'] is None  or 'None' in lis['social_flags']['facebookAltTitle'] ):
				print 'nome = title'
				lis['lead']['name'] = lis['social_flags']['facebookAltTitle'].strip()
			else:
				print 'facebookAltTitle is None'

			print ' in mezzo'

			if not (lis['social_flags']['facebookAltDesc'] is None or 'None' in lis['social_flags']['facebookAltDesc'] ):
				print 'desc = desc'
				lis['lead']['description'] = lis['social_flags']['facebookAltDesc'].strip() 
			else:
				print 'facebookAltDesc is None'


			print lis['lead']

			if not (lis['social_flags']['facebookText'] is None):
				msg = lis['social_flags']['facebookText'].strip()
			else:
				print 'facebookText is None'
			print 'try msg del Facebook = ' + msg
		except:
			print 'except  msg del Facebook = ' + msg
			return [ None, '', '','']
	else:
		try:
			print 'in TW'
			abs_account = pyTwitterClass.TwitterConnection( da_fare_under )
			print ' preso account ' + str(abs_account)

			print lis['lead']

			# e devo anche mettere il testo giusto
			print ' sta per strippare '
			if lis['social_flags']['twitterText'] is None:
				print 'lis e NONE'
			if not (lis['social_flags']['twitterText'] is None or not (lis['social_flags']['twitterText'])):
				msg = Croppa_Titolo( lis['social_flags']['twitterText'].strip() , lis['link'])
				print ' 1 - setto msg = ' + msg

			else:
				# devo verificare su richiesta di Diego se ho l upload dell immagine
				# nel qual caso devo metterci il titolo  + url breve + sottotitolo
				print ' nella verifica della social flag'
				if  not (lis['social_flags']['tw-uploadImage'] is None ) and 'true' in lis['social_flags']['tw-uploadImage'] :
					print ' tw-uploadImage true '
					#print lis['link']
					#print lis['lead']['description']
					#print lis['title']

					if lis['lead']['description'] is None:
						print 'description is NONE'
						desc = ''
					else:
						desc = lis['lead']['description']
					#print ' --- ' 
					msg = Componi_Testo_Limite( lis['lead']['name'] , lis['lead']['link'] , desc )
					print ' 2 - setto msg = ' + msg
					#print ' --- ' 
				
				else:
					# avendo cambiato il testo del msg non posso mettere SEMPRE la url in fondo
					# quindi e qui che devo aggiungerla o meno in fondo al msg
					print ' tw-uploadImage false '
					# se saimo una short story dopbbiamo mettere il titolo nel testo
					if 'short' in lis['contenttype']:
						msg = Croppa_Titolo( lis['title'] , lis['lead']['link'])
						print ' 3 - setto msg = ' + msg
					else:
						msg = Croppa_Titolo( '' ,  lis['lead']['link'] )
						print ' 4 - setto msg = ' + msg
	
			print 'try msg del TWitt = '
			print msg 
			
		except:
			print 'except msg del TWitt = ' 
			print msg
			return [ None, []]

	print lis['lead']
	print ' ------------- FINE  ChooseAccount --------------------------------------- '
	return [ abs_account, [ msg, '', '', ast.literal_eval(str(lis['lead'])) ] ]

def Aumenta_Error( val ):
	
	if val is None or ( not ( 'Errore' in val )) :
		return 'Errore 1'
	else:
		numero = val.split(':')[0]
		numero = numero.split('Errore')[-1]
		print numero.strip()
	
	return 'Errore ' +  str( int(numero) + 1)


def SettaValue( result, lis, da_fare, _id ):

	if result:
		lis['social_flags'][da_fare.replace('account', 'sent')] =  _id
		print 'aggiunto a lista' + str(da_fare.replace('account', 'sent')) +' '+  str(lis['social_flags'][da_fare.replace('account', 'sent')])
	else:
		if '403' in str(_id):
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) + ': tweet duplicato'
			print 'da ritornare il motivo per cui non riusciamo a postare\n\n'
		else:
			lis['social_flags'][da_fare.replace('account', 'sent')] =  Aumenta_Error( lis['social_flags'][da_fare.replace('account', 'sent')]) +':' +  str(_id).split(',')[-1] 
			print 'da ritornare il motivo per cui non riusciamo a postare\n\n'

def SettaEnvironment( debug ):

	if (debug):
		print 'PRIMA DI SETTARE ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ

	if (debug):
		print '---------------------------------'

	for param in pySettaVars.Environment_Variables.keys():
		#print "%20s %s" % (param,dict_env[param])
		os.environ[param] = pySettaVars.Environment_Variables[ param ]


	if (debug):
		print 'DOPO AVER SETTATO ENVIRONMENT'
	if (debug):
		print 'ENV : '
	if (debug):
		print os.environ



def PreparaFB( item ):

	print " ---------------------- INIZIO PreparaFB  ------------------ " 
	print item
	# per Preparare lo stream devo :
	# 1 - FB.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )

	t_start = item['SocialFlags']['t_start']
	
	print datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s")

	_fb_livestreaming_details = {}
	

	for _fb_accounts in item['SocialFlags']['fb-livestreaming-account']:

		print ' giro in PreparaFB con account = ' + _fb_accounts

		result_create = pyFacebookClass.Create_Scheduled_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ], datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s") , _fb_accounts )

		# qui inizializzo la VIDEODETAILS
		_fb_livestreaming_details['VideoDetails'+ _fb_accounts] = result_create
		print '----------------------------------- first VideoDetails ---------------------------'
		print  _fb_livestreaming_details['VideoDetails'+ _fb_accounts ]
		print '----------------------------------- first VideoDetails ---------------------------'

		# 2 - passare la secure_stream_url alla create_mapentry del WOW
		# cambio lo stream name per essere quella giusta da mandare alla lib Wow
		# per adesso assumo che il Wirecast sia sempre attivo
		# poi mi preoccupero di accenderlo
		_fb_livestreaming_details['VideoDetails'+ _fb_accounts]['wow_secure_stream'] =  _fb_livestreaming_details['VideoDetails'+ _fb_accounts]['secure_stream_url'].split('443/rtmp/')[-1]
		_fb_livestreaming_details['VideoDetails'+ _fb_accounts]['wow_name'] =  item['ECEDetails']['id'] + '_' + _fb_livestreaming_details['VideoDetails'+ _fb_accounts]['id']


		wow = pyWowzaClass.Create_Mapentry_FB( _fb_livestreaming_details['VideoDetails'+ _fb_accounts]['wow_name'] , _fb_livestreaming_details['VideoDetails'+ _fb_accounts]['wow_secure_stream']  )


		print wow
		if wow['success'] :
			# 3 - se ritornato success : true
			# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

			# qui inizializzo la WOWZADETAILS
			_fb_livestreaming_details['WowzaDetails'+ _fb_accounts] = pyWowzaClass.Get_Wowza_Mapentry_Details( _fb_livestreaming_details['VideoDetails'+ _fb_accounts]['wow_name' ] )
			# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
			# o per la semplice Delete_Mapentry( nome )


		# e adesso a seconda del valore della flag fbSenzaNotifica scelgo se fare la notifica 
		# oppure no
		if ( not  ( item['SocialFlags']['fbSenzaNotifica'] is None ) and 'true' in item['SocialFlags']['fbSenzaNotifica'] ):
			print ' sono in Senza Notifica e quindi torno senza prenotare' 
			return item


		print " ---------------------- DEBUG PreparaFB  ------------------ "
		print item['SocialFlags']
		print " ---------------------- DEBUG PreparaFB  ------------------ "

	
	item['FBStreamingDetails'] = _fb_livestreaming_details


	print " ---------------------- DEBUG PreparaFB  ------------------ "
	print item['FBStreamingDetails']
	print " ---------------------- DEBUG PreparaFB  ------------------ "
	for key,value in item['FBStreamingDetails'].iteritems():
		print key,value
	print " ---------------------- DEBUG PreparaFB  ------------------ "

	# clean per il test chiama le delete delle entry create
	#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
	#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] )
	#print wow_delete
	# DA TOGLIERE

	print " ---------------------- FINE PreparaFB  ------------------ "
	return item


def Prendi_Id_StreamName( VideoDetailsDictionary ):
	result = None
	id = None

	if 'LivestreamsDetails' in VideoDetailsDictionary and 'cdn' in VideoDetailsDictionary['LivestreamsDetails'] and  \
		'ingestionInfo' in VideoDetailsDictionary['LivestreamsDetails']['cdn'] and 'streamName' in VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']:
		print 'streamname = ' + VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName']
		result = VideoDetailsDictionary['LivestreamsDetails']['cdn']['ingestionInfo']['streamName']

	if 'BindDetails' in VideoDetailsDictionary and 'id' in VideoDetailsDictionary['BindDetails']:
		print 'bind Id = ' + VideoDetailsDictionary['BindDetails']['id']
		id = VideoDetailsDictionary['BindDetails']['id']

	return [ result , id ]



def PreparaYT( item ):

	print " ---------------------- INIZIO PreparaYT  ------------------ " 
	print item
	# per Preparare lo stream devo :
	# 1 - YT.Create_Live_Video( title, description ) che mi torna	
	# {
    	# "secure_stream_url": "rtmps://rtmp-api.facebook.com:443/rtmp/1471030776261970?ds=1&s_l=1&a=ATh1bGN96LhhKx8S",
    	# "id": "1471019276263120"
	# ..... 
	# oltre a tutto il resto
	# }

	#result_create = pyFacebookClass.Create_Live_Video( item['SocialFlags']['fbVideoTitle' ] , item['SocialFlags']['fbVideoDesc' ]  )
	t_start = item['SocialFlags']['t_start']
	
	print datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime("%s")

	_yt_livestreaming_details = {}

	for _yt_accounts in item['SocialFlags']['yt-livestreaming-account']:

		print ' giro in PreparaYT con account = ' + _yt_accounts

		# CLAD DA RIPRISTINARE
		result_create = pyYoutubeClass.Crea_And_Bind_Livestream_Event( item['SocialFlags']['ytVideoTitle' ] , item['SocialFlags']['ytVideoDesc' ], datetime.strptime(t_start, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%dT%H:%M:%S.000Z') , 'private', _yt_accounts )

		#result_create = {u'LivestreamsDetails': {u'status': {u'streamStatus': u'ready', u'healthStatus': {u'status': u'noData'}}, u'kind': u'youtube#liveStream', u'cdn': {u'resolution': u'1080p', u'ingestionType': u'rtmp', u'ingestionInfo': {u'ingestionAddress': u'rtmp://a.rtmp.youtube.com/live2', u'streamName': u't2fp-cwzg-z646-2zk0', u'backupIngestionAddress': u'rtmp://b.rtmp.youtube.com/live2?backup=1'}, u'frameRate': u'30fps', u'format': u'1080p'}, u'snippet': {u'isDefaultStream': False, u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'description': u'Test_Description', u'publishedAt': u'2017-06-01T13:50:02.000Z', u'title': u'Stream_per_Test_CLAD.2.0'}, u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/2aA49S91KnshlTkN2tMSEmcESAE"', u'id': u'v7yEZoifGFg_CioakqBMFA1496325002526255'}, u'BindDetails': {u'contentDetails': {u'closedCaptionsType': u'closedCaptionsDisabled', u'projection': u'rectangular', u'startWithSlate': False, u'boundStreamId': u'v7yEZoifGFg_CioakqBMFA1496325002526255', u'enableEmbed': False, u'enableClosedCaptions': False, u'enableLowLatency': False, u'boundStreamLastUpdateTimeMs': u'2017-06-01T13:50:02.537Z', u'enableContentEncryption': False, u'recordFromStart': True, u'enableDvr': True, u'monitorStream': {u'broadcastStreamDelayMs': 0, u'embedHtml': u'<iframe width="425" height="344" src="https://www.youtube.com/embed/zEa_vQr6v7A?autoplay=1&livemonitor=1" frameborder="0" allowfullscreen></iframe>', u'enableMonitorStream': True}}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/rPXg9ooOujATuCxHtve5Tt0Pqmg"', u'id': u'zEa_vQr6v7A'}, u'LivebroadcastDetails': {u'snippet': {u'thumbnails': {u'default': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/default_live.jpg?sqp=CIi3wMkF&rs=AOn4CLD-OH957yBGBrNOZB1s-_Wqe8gq0Q', u'width': 120, u'height': 90}, u'high': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/hqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLBi97H8jZLxX3S1EHXKTiuZijOMtg', u'width': 480, u'height': 360}, u'medium': {u'url': u'https://i9.ytimg.com/vi/zEa_vQr6v7A/mqdefault_live.jpg?sqp=CIi3wMkF&rs=AOn4CLB_LuQaBEamvFwnHdAGGy3Zun6Fdg', u'width': 320, u'height': 180}}, u'title': u'Broad_per_Test_CLAD.2.0', u'channelId': u'UCv7yEZoifGFg_CioakqBMFA', u'publishedAt': u'2017-06-01T13:50:03.000Z', u'liveChatId': u'Cg0KC3pFYV92UXI2djdB', u'scheduledStartTime': u'2017-06-01T22:00:00.000Z', u'isDefaultBroadcast': False, u'description': u'Create_Broadcast_Description'}, u'status': {u'recordingStatus': u'notRecording', u'privacyStatus': u'private', u'lifeCycleStatus': u'created'}, u'kind': u'youtube#liveBroadcast', u'etag': u'"m2yskBQFythfE4irbTIeOgYYfBU/ikqqsSV_d94FW2DIvGSVREyQa4E"', u'id': u'zEa_vQr6v7A'}}


		# qui inizializzo la VIDEODETAILS con un dizionario che mi arriva dalla create e bind di youtube
		# formata come segue:  { 'LivestreamsDetails':livestreams_res,'LivebroadcastDetails':livebroadcast_res,'BindDetails':bind_livebroadcast}
		_yt_livestreaming_details['VideoDetails'+ _yt_accounts] = result_create
		print '----------------------------------- first VideoDetails ---------------------------'
		print  _yt_livestreaming_details['VideoDetails'+ _yt_accounts ]
		print '----------------------------------- first VideoDetails ---------------------------'

		# 2 - passare la secure_stream_url alla create_mapentry del WOW
		# cambio lo stream name per essere quella giusta da mandare alla lib Wow
		# per adesso assumo che il Wirecast sia sempre attivo
		# poi mi preoccupero di accenderlo
		[ streamName , details_id ] = Prendi_Id_StreamName( _yt_livestreaming_details['VideoDetails'+ _yt_accounts] )
		_yt_livestreaming_details['VideoDetails'+ _yt_accounts]['wow_secure_stream'] = streamName 
		_yt_livestreaming_details['VideoDetails'+ _yt_accounts]['wow_name'] =  item['ECEDetails']['id'] + '_' + details_id
		wow = pyWowzaClass.Create_Mapentry_YT( _yt_livestreaming_details['VideoDetails'+ _yt_accounts]['wow_name'] , _yt_livestreaming_details['VideoDetails'+ _yt_accounts]['wow_secure_stream']  )
		print wow
		if wow['success'] :
			# 3 - se ritornato success : true
			# 4 - chiederne i details con Get_Wowza_Mapentry_Details da salvare in lis 

			# qui inizializzo la WOWZADETAILS
			_yt_livestreaming_details['WowzaDetails'+ _yt_accounts] = pyWowzaClass.Get_Wowza_Mapentry_Details( _yt_livestreaming_details['VideoDetails'+ _yt_accounts]['wow_name' ] )
			# per successive modifiche con Action_Mapentry( nome, 'enable/disable')
			# o per la semplice Delete_Mapentry( nome )


		print " ---------------------- DEBUG PreparaYT  ------------------ "
		print item['SocialFlags']
		print " ---------------------- DEBUG PreparaYT  ------------------ "
	
	item['YTStreamingDetails'] = _yt_livestreaming_details


	print " ---------------------- DEBUG PreparaYT  ------------------ "
	print item['YTStreamingDetails']
	print " ---------------------- DEBUG PreparaYT  ------------------ "
	for key,value in item['YTStreamingDetails'].iteritems():
		print key,value
	print " ---------------------- DEBUG PreparaYT  ------------------ "



	# clean per il test chiama le delete delle entry create
	#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
	#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] )
	#print wow_delete
	# DA TOGLIERE

	print " ---------------------- FINE PreparaYT  ------------------ "
	return item

def VerificaON( item ):

	if len(item['FBStreamingDetails']) > 0:

		if 'VideoDetails' + item['SocialFlags']['fb-livestreaming-account'][0] in item['FBStreamingDetails']:
			print item['FBStreamingDetails']['VideoDetails' + item['SocialFlags']['fb-livestreaming-account'][0]]['status']
			if 'LIVE' == item['FBStreamingDetails']['VideoDetails' + item['SocialFlags']['fb-livestreaming-account'][0]]['status']:
				print ' in VerificaON siamo LIVE !! '
				return True
	
	# per guardare se e' on cerco nel Video Details se lo stato e
	# LIVE altrimenti
	if len(item['YTStreamingDetails']) > 0:


		if not 'VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0] in item['YTStreamingDetails']:
			# poi magari questa e condizione di cancellazione
			return False

		if not 'LivebroadcastDetails' in item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]:
			return False
		if not 'status' in item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']:
			return False
		if not 'lifeCycleStatus' in item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']:
			return False

		print item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus']
		if 'live' in item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus']:
			print ' in VerificaON siamo LIVE !! '
			return True
		
	return False

def AccendiFB( item ):

	print " ---------------------- INIZIO AccendiFB  ------------------ "
	print item
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'LIVE' == item['FBStreamingDetails']['VideoDetails' + item['SocialFlags']['fb-livestreaming-account'][0]]['status'] :
		print ' in AccendiFB siamo LIVE !! '
		return item


	# se sono qui devo accendere il Wow


	for _fb_accounts in item['SocialFlags']['fb-livestreaming-account']:

		print ' giro in AccendiFB con account = ' + _fb_accounts

		result_enable = pyWowzaClass.Action_Mapentry( item['FBStreamingDetails']['VideoDetails' +  _fb_accounts]['wow_name' ] , 'enable')

		if result_enable['success']:
			item['FBStreamingDetails']['WowzaDetails' +  _fb_accounts]['enabled'] = True

		result_update = pyFacebookClass.Update_Live_Video_Details(  item['FBStreamingDetails']['VideoDetails' +  _fb_accounts]['id'], "status", "LIVE_NOW" , _fb_accounts )

		if 'error' in result_update:
			item['FBStreamingDetails']['VideoDetails' +  _fb_accounts]['status'] = 'LIVE_STOPPED'
			return item


		item['FBStreamingDetails']['VideoDetails' + _fb_accounts ]['status'] = result_update['status']
		print " ---------------------- DEBUG AccendiFB  ------------------ "
		print "item['FBStreamingDetails']['VideoDetails' +  _fb_accounts] "
		print item['FBStreamingDetails']['VideoDetails' +  _fb_accounts]


	print " ---------------------- FINE AccendiFB  ------------------ "
	return item


def wait_until(somepredicate, timeout, period=0.25, *args, **kwargs):

	mustend = time.time() + timeout
	while time.time() < mustend:
		if somepredicate(*args, **kwargs): 
			return True
		time.sleep(period)
	return False

def AccendiYT( item ):

	print " ---------------------- INIZIO AccendiFB  ------------------ "
	print item
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'live' == item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus'] :
		print ' in AccendiYT siamo LIVE !! '
		return item


	# se sono qui devo accendere il Wow


	for _yt_accounts in item['SocialFlags']['yt-livestreaming-account']:

		print ' giro in AccendiYT con account = ' + _yt_accounts

		result_enable = pyWowzaClass.Action_Mapentry( item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['wow_name' ] , 'enable')

		if result_enable['success']:
			item['YTStreamingDetails']['WowzaDetails' +  _yt_accounts]['enabled'] = True


		# qui per accendere devo verificare ( con timeout ? ) che il livestreaming sia active
		# e quindi settare poi il broadcast 

		args = ()
		kwargs = { 'status_param' : 'active', 'item_id' : item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['LivestreamsDetails']['id'], 'account' : _yt_accounts }
		if not wait_until( pyYoutubeClass.Check_LiveStream_Status, 10, 0.25,*args, **kwargs ):
			continue

		
		# prima a testing
		result_update = pyYoutubeClass.Update_LiveBroadcast(  item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['LivebroadcastDetails']['id'], "testing" , _yt_accounts )

		# verificare che sia veramente a testing
		args = ()
		kwargs = { 'status_param' : 'testing', 'item_id' : item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['LivebroadcastDetails']['id'], 'account' : _yt_accounts }
		if not wait_until( pyYoutubeClass.Check_LiveBroadcast_Status, 15, 0.25,*args, **kwargs ):
			continue
	
		# e quindi a live
		result_update = pyYoutubeClass.Update_LiveBroadcast(  item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['LivebroadcastDetails']['id'], "live" , _yt_accounts )

		print result_update

		if 'error' in result_update:
			item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]['status'] = 'complete'
			return item


		item['YTStreamingDetails']['VideoDetails' + _yt_accounts ]['LivebroadcastDetails'] = result_update
		print " ---------------------- DEBUG AccendiFB  ------------------ "
		print "item['VideoDetails' +  _yt_accounts] "
		print item['YTStreamingDetails']['VideoDetails' +  _yt_accounts]


		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['SocialFlags']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['VideoDetails']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['WowzaDetails']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "


		# clean per il test chiama le delete delle entry create
		#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
		#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] )
		#print wow_delete
		# DA TOGLIERE

	print " ---------------------- FINE AccendiFB  ------------------ "
	return item

def SpegniFB( item ):

	print " ---------------------- INIZIO SpegniFB  ------------------ "
	print item
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'LIVE_STOPPED' == item['FBStreamingDetails']['VideoDetails' + item['SocialFlags']['fb-livestreaming-account'][0]]['status'] :
		print ' in SpegniFB siamo STOPPATI !! '
		return item

	# se sono qui devo accendere il Wow

	for _fb_accounts in item['SocialFlags']['fb-livestreaming-account']:

		result_disable = pyWowzaClass.Action_Mapentry( item['FBStreamingDetails']['VideoDetails' + _fb_accounts]['wow_name' ] , "disable")
		if result_disable['success']:
			item['FBStreamingDetails']['WowzaDetails' + _fb_accounts]['enabled'] = False

		result_update = pyFacebookClass.Update_Live_Video_Details( item['FBStreamingDetails']['VideoDetails' + _fb_accounts]['id'] , "end_live_video", "true", _fb_accounts )
		if 'error' in result_update:
			item['FBStreamingDetails']['VideoDetails' + _fb_accounts]['status'] = 'LIVE_STOPPED'
			return item
		item['FBStreamingDetails']['VideoDetails' + _fb_accounts]['status'] = result_update['status']

		item['FBStreamingDetails']['WowzaDetails' + _fb_accounts]['WowDeleted'] = pyWowzaClass.Delete_Mapentry( item['FBStreamingDetails']['VideoDetails' + _fb_accounts]['wow_name' ] )
		


	print " ---------------------- FINE SpegniFB  ------------------ "
	return item


def SpegniYT( item ):

	print " ---------------------- INIZIO SpegniYT  ------------------ "
	print item
	# per Accendere lo stream devo :
	# devo verificare che non stia gia andando ....
	# e quindi accendere il WIRECAST ( se spento )
	# e abilitare il Wowza con Action_Mapentry( nome, 'enable/disable') la entry opportuna
	if 'complete' == item['YTStreamingDetails']['VideoDetails' + item['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus'] :
		print ' in SpegniYT siamo STOPPATI !! '
		return item

	# se sono qui devo accendere il Wow

	for _yt_accounts in item['SocialFlags']['yt-livestreaming-account']:

		result_disable = pyWowzaClass.Action_Mapentry( item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['wow_name' ] , "disable")
		if result_disable['success']:
			item['YTStreamingDetails']['WowzaDetails' + _yt_accounts]['enabled'] = False

		result_update = pyYoutubeClass.Update_LiveBroadcast( item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['LivebroadcastDetails']['id'] , "complete", _yt_accounts )


		if 'error' in result_update:
			item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['LivebroadcastDetails']['status']['lifeCycleStatus'] = 'complete'
			item['YTStreamingDetails']['WowzaDetails' + _yt_accounts]['WowDeleted'] = pyWowzaClass.Delete_Mapentry( item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['wow_name' ] )
			return item
		item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['LivebroadcastDetails'] = result_update

		item['YTStreamingDetails']['WowzaDetails' + _yt_accounts]['WowDeleted'] = pyWowzaClass.Delete_Mapentry( item['YTStreamingDetails']['VideoDetails' + _yt_accounts]['wow_name' ] )
		

		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['SocialFlags']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['VideoDetails']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "
		#print item['WowzaDetails']
		#print " ---------------------- DEBUG AccendiFB  ------------------ "


		# clean per il test chiama le delete delle entry create
		#pyFacebookClass.Delete_Live_Video( item['VideoDetails']['id'])
		#wow_delete = pyWowzaClass.Delete_Mapentry( item['VideoDetails']['wow_name'] )
		#print wow_delete
		# DA TOGLIERE

	print " ---------------------- FINE SpegniYT  ------------------ "
	return item





def GestisciPREPARA( dict_items ):

	print " ---------------------- Inizio GestisciPREPARA  ------------------ "
	result_dict = {}
		
	# devo verificare che sia gia stato preparato
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		#print value
		if len(value['SocialFlags']['fb-livestreaming-account']) > 0:
			if not 'FBStreamingDetails' in value or not 'VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
				print ' gestisciPrepara dice che devo farlo FB'
				value = PreparaFB( value )
			result_dict[key] =  value 


	# devo verificare che sia gia stato preparato
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
			if not 'FBStreamingDetails' in value or not 'VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0] in value['YTStreamingDetails']:
				print ' gestisciPrepara dice che devo farlo  YT'
				value = PreparaYT( value )
			result_dict[key] =  value 

	print " ---------------------- Fine GestisciPREPARA  ------------------ "
	return result_dict


def GestisciON( dict_items ):

	print " ---------------------- Inizio GestisciON  ------------------ "
	result_dict = {}

	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		if len(value['FBStreamingDetails']) > 0:
			if not 'VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
				value = PreparaFB( value )
			else:
				if not VerificaON( value ):
					value = AccendiFB( value )
			result_dict[key] = value 	



	# devo verificare che sia gia stato acceso
	# altrimenti accenderlo
	for key, value in dict_items.iteritems():
		if len(value['YTStreamingDetails']) > 0:
			if not 'VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0] in value['YTStreamingDetails']:
				value = PreparaYT( value )
			else:
				if not VerificaON( value ):
					value = AccendiYT( value )
			result_dict[key] = value 	

	print " ---------------------- Fine GestisciON  ------------------ "
	return result_dict


def GestisciOFF( dict_items ):

	print " ---------------------- Inizio GestisciOFF  ------------------ "
	result_dict = {}
	da_archiviare_dict = {}

	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['FBStreamingDetails']) > 0:

			if not 'VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0] in value['FBStreamingDetails']:
				# poi magari questa e condizione di cancellazione
				continue
			if value['FBStreamingDetails']['VideoDetails' + value['SocialFlags']['fb-livestreaming-account'][0]]['status'] == 'LIVE_STOPPED' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniFB( value )

			result_dict[key] = value 	



	# devo verificare che sia gia stato spento
	# altrimenti spegnerlo
	for key, value in dict_items.iteritems():
		if len(value['YTStreamingDetails']) > 0:

			if not 'VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0] in value['YTStreamingDetails']:
				# poi magari questa e condizione di cancellazione
				continue

			if not 'LivebroadcastDetails' in value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]:
				continue
			if not 'status' in value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']:
				continue
			if not 'lifeCycleStatus' in value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']:
				continue

			print 'liveBroadcast status : '

			print  value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus']
				# poi magari questa e condizione di cancellazione
			if value['YTStreamingDetails']['VideoDetails' + value['SocialFlags']['yt-livestreaming-account'][0]]['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete' :
				# per adesso lo metto ancora 
				# poi magari questa e condizione di cancellazione
				da_archiviare_dict[key] = value 	
				continue
			else:
				value = SpegniYT( value )

			result_dict[key] = value 	

	print " ---------------------- Fine GestisciOFF  ------------------ "
	return [ result_dict, da_archiviare_dict ]


def Merge_Liste( da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF):
	
	da_fare_list_PREPARA.update(da_fare_list_ON)
	da_fare_list_PREPARA.update(da_fare_list_OFF)

	return da_fare_list_PREPARA



if __name__ == "__main__":


	'''
	# test connessione account facebook
        print 'comincio'
        fb_account = pyFacebookClass.FacebookConnection( 'fb_account_rsiTest' )
        print 'account' + str(fb_account)
        if not(fb_account is None):
		print ' PASS '
                print fb_account.PageId
                print 'ok'
        print ' adesso posto'
        print fb_account.post( 'test msg per facebook : ciaooooo ')
	#fine test connessione facebook
        #exit(0)
	'''

	# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
	SettaEnvironment( False )


	# prende contenuti da ECE
	lista_content = {}
	lista_content.update(  Crea_List_Livestreaming.Solr_Prendi_Dict_From_Rsi( '4' ) )
	# qui arrivano records del tipo : 
	# lista_content = [{'lastmodifieddate': '2017-01-10T13:23:23Z', 'state': 'published', 'contenttype': 'story', 'id': 'article:8568391', 'title': 'test story title'}]

	#print lista_content

	# CLAD
	# a queste devo aggiungere quelle del DB persistente con quelli programmati ma ancora non fatti
	# o non ancora attivati o cmq da controllare
	# qui devo prendere gli eventuali items messi nel db per verifiche successive
	# print os.environ['DB_NAME']
	print '-----------------------  prima di APICoreX/API_DB.py.Aggiungi_Items_Db ---------------------'
        lista_content.update(API_DB.Aggiungi_Items_da_Db( os.environ['DB_NAME'] , lista_content))
	
	#print lista_content

	# adesso devo scremare la lista con tutti e SOLO quelli che hanno il social
	# flaggato per la pubblicazione
	# ma non sono ancora stati mandati cioe non hanno nulla nel campo sent
	# oppure hanno la flag di resend settata a True
	[ da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF ] = Prendi_Content_Da_ECE.Controlla_Flag_Social( lista_content )

	
	# ho gli items nella forma :
	# {'contenttype': 'story', 'lead': {'picture': 'https://www.rsi.ch/rsi-api/resize/image/WEBVISUAL/413975', 'caption': '', 'link': 'http://www.rsi.ch', 'name': 'test push to social', 'description': 'None'}, 'title': 'test story title', 'lastmodifieddate': '2017-01-10T13:23:23Z', 'social_flags': {'fb-account-rsiSport': 'false', 'fb-sent-rsiGlobal': None, 'fb-account-rsiNews': 'false', 'facebookAltDesc': 'Descrizione Alt', 'tw-sent-rsiSport': None, 'tw-sent-rsiTest': None, 'fb-sent-rsiTest': None, 'tw-account-rsiTest': 'true', 'tw-account-rsiNews': 'false', 'fb-sent-rsiSport': None, 'tw-sent-rsiNews': None, 'fb-account-rsiGlobal': 'false', 'fb-sent-rsiNews': None, 'facebookAltTitle': 'Titolo Alternativo', 'fb-account-rsiTest': 'true', 'twitterText': 'Testo del Tweet di prova 1 ', 'tw-account-rsiSport': 'false', 'facebookText': 'Testo del post Facebook 1'}, 'state': 'published', 'da_fare': ['tw-account-rsiTest', 'fb-account-rsiTest'], 'id': 'article:8568391'}
	# se ho trovato una picture nel lead, altrimenti senza il campo lead
	
	da_fare_list_PREPARA = GestisciPREPARA( da_fare_list_PREPARA )
	da_fare_list_ON = GestisciON( da_fare_list_ON )
	[ da_fare_list_OFF, da_archiviare_list_OFF ] = GestisciOFF( da_fare_list_OFF )

	
	lista_per_db = Merge_Liste( da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF)
	
	# li scrivo dopo aver fatto le operazioni di ON e OFF che cambiano lo stato 
	# dei video details e quindi dopo un merge sulla lista da buttare fuori
	API_DB.Scrivi_Items_Db( os.environ['DB_NAME'], lista_per_db )
	API_DB.Aggiungi_Items_al_Db( os.environ['ARCHIVE_NAME'], da_archiviare_list_OFF )
			
	
	print '---------------------------------------- dopo di Prendi_Content_Da_ECE.Scrivi_Items_Db ---------------------'
	#print lista_per_db
	#lista_content =  Prendi_Content_Da_ECE.Update_Flag_Social( lista_content )
	

