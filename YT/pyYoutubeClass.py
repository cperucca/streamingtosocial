#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Keys.Json_Keys as Json_Keys

import urllib2
import urllib
import requests
import json
import ast
import logging
import time
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True



'''
esempio di url per prendere il code della prima autorizzazione
https://accounts.google.com/o/oauth2/auth?client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scope=https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.upload&response_type=code&access_type=offline

ottenuto il seguente token 
code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU

_url_ = 'https://accounts.google.com/o/oauth2/auth?\
client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&\
redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&\
scope=https%3A//www.googleapis.com/auth/youtube%20https%3A//www.googleapis.com/auth/youtube.upload&\
response_type=code&\
access_type=offline'

data = urllib.urlencode( _url_ )
print data
req = urllib2.Request( _url_ )

result = urllib2.urlopen(req)
print  result.read()
# result.info() will contain the HTTP headers


e adesso devo fare una POST 

code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU
client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&
client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&
redirect_uri=http://localhost/oauth2callback&
grant_type=authorization_code


code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU&client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&redirect_uri=http://localhost/oauth2callback&grant_type=authorization_code

'''

def Get_Info_Access_Token( access_token ): 

	print(' ------------------ INIZIO Get_Info_Access_Token ----------------------')
	url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=' + access_token

	print url

	request = urllib2.Request(url)
	
	print 'OUTGOING DATA:'
	print request.get_data()

	print
	print 'SERVER RESPONSE:'
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	print(' ------------------ FINE Get_Info_Access_Token ----------------------')

def Chiama_Refresh_Accesso_Token_Debug( refresh_token ):

	print(' ------------------ INIZIO Chiama_Refresh_Accesso_Token ----------------------')
	url = 'https://accounts.google.com/o/oauth2/token'
	print 'curl --data "' +  'client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&refresh_token=1/EQiYRrwMDa5DrhQkNEoOq23zaxss7eVOqDGZgH18G-E&grant_type=refresh_token' + '" ' + url

	print(' ------------------ PRIMA CHIAMATA POST per REFRESH TOKEN ----------------------')

	query_args = { 'client_id':'567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com', 
			'client_secret':'sRLqC0TsUGlOZh3gCFlVOC6-',
			'refresh_token':refresh_token,
			'grant_type':'refresh_token' }

	request = urllib2.Request(url)
	print 'Request method before data:', request.get_method()
	request.add_data(urllib.urlencode(query_args))
	print 'Request method after data :', request.get_method()

	print
	print 'OUTGOING DATA:'
	print request.get_data()

	print
	print 'SERVER RESPONSE:'
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	print(' ------------------ DOPO CHIAMATA POST per REFRESH TOKEN ----------------------')

	result_dict = json.loads(result_dict)

	print
	print 'VALID ACCESS TOKEN :'
	print result_dict['access_token']
	return result_dict['access_token']
	print(' ------------------ FINE Chiama_Refresh_Accesso_Token ----------------------')




def Chiama_Refresh_Accesso_Token( refresh_token ):

	#print(' ------------------ INIZIO Chiama_Refresh_Accesso_Token ----------------------')
	url = 'https://accounts.google.com/o/oauth2/token'
	query_args = { 'client_id':'567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com', 
			'client_secret':'sRLqC0TsUGlOZh3gCFlVOC6-',
			'refresh_token':refresh_token,
			'grant_type':'refresh_token' }

	request = urllib2.Request(url)
	request.add_data(urllib.urlencode(query_args))
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	result_dict = json.loads(result_dict)
	return result_dict['access_token']




#refresh_token = '1/q2AG0bOHbhHkOvbtCpV6BJpxoi2bFDZ52EXnKpgUV8E'
#access_token = Chiama_Refresh_Accesso_Token( refresh_token ) 
#Get_Info_Access_Token( access_token )
#
#exit(0)





def Chiama_Secondo_Token( code ): 

	print(' ------------------ INIZIO Chiama_Secondo_Token ----------------------')
	url = 'https://accounts.google.com/o/oauth2/token'

	values = { 'code':code,
	'client_id':'567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com',
	'client_secret':'sRLqC0TsUGlOZh3gCFlVOC6-',
	'redirect_uri':'urn:ietf:wg:oauth:2.0:oob',
	'grant_type':'authorization_code' }

	print url, values
	data = urllib.urlencode(values)

	request = urllib2.Request(url)
	
	print 'Request method before data:', request.get_method()
	request.add_data(data)
	print 'Request method after data :', request.get_method()

	print 'curl --data "' +  'code=' + code + '&client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&redirect_uri=urn:ietf:wg:oauth:2.0:oob&grant_type=authorization_code' + '" ' + url
	print '\n\n'

	return

	print
	print 'OUTGOING DATA:'
	print request.get_data()

	print
	print 'SERVER RESPONSE:'
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	result_dict = ast.literal_eval(result_dict)
	return result_dict['refresh_token']
	print(' ------------------ FINE Chiama_Secondo_Token ----------------------')


#Chiama_Secondo_Token( '4/RFhy8XP4sgEX75iXUOXY-mUHM_D7s0PSAv9K9WUNVPg' )

#exit(0)

''' ricevuto 
{
  "access_token" : "ya29.Ci9uA0EmE_yNHKaGBN6UX4hLncYM7UIvXQhLW1onPEVX6uEzcE4HqxkBssq9ZcK8MA",
  "token_type" : "Bearer",
  "expires_in" : 3600,
  "refresh_token" : "1/EQiYRrwMDa5DrhQkNEoOq23zaxss7eVOqDGZgH18G-E"
}

'''
# refreshing the access token

''' 
client_id=21302922996.apps.googleusercontent.com&
client_secret=XTHhXh1SlUNgvyWGwDk1EjXB&
refresh_token=1/6BMfW9j53gdGImsixUH6kU5RsR4zwI9lUVX-tqf8JXQ&
grant_type=refresh_token
'''


def Stampa_info_LiveBroadcast(it):

	print it['id']
	snip = it['snippet']
	status = it['status']
	details = it['contentDetails']

	print snip['title'],snip['publishedAt']
	print status['privacyStatus'], status['recordingStatus']
	if 'boundStreamId' in details:
		print details['boundStreamId']
	print  details['enableDvr']
	print '------------'

def Parse_Result_Json_LiveStream( result ):
	print " ---------------------- INIZIO Parse_Result_Json LiveStream ------------------ " 

	if type(result) in (str, unicode):
		result =  json.loads(result)
	print result['kind']
	if 'List' not in result['kind']:
		return
	
	print len( result['items'] )
	if len( result['items'] ) > 0:
		for it in result['items']:
			print it['kind']
			print it['id']
			snip = it['snippet']
			print snip['title']
			print '\n\r'
				
	print " ---------------------- FINE Parse_Result_Json LiveStream ------------------ " 



def Parse_Result_Json_LiveBroadcast( result ):

	print " ---------------------- INIZIO Parse_Result_Json LiveBroadcast ------------------ " 
	if type(result) in (str, unicode):
		print 'type = string'
		#print result

		result =  json.loads(result)
	#print result['kind']
	if 'List' not in result['kind']:
		return
	
	print len( result['items'] )
	if len( result['items'] ) > 0:
		for it in result['items']:
			#print it['kind']
			#print it['id']
			Stampa_info_LiveBroadcast(it)
				

	print " ---------------------- FINE Parse_Result_Json LiveBroadcast ------------------ " 



def Parse_Result_Json( result ):

	print " ---------------------- INIZIO Parse_Result_Json ------------------ " 

	if type(result) in (str, unicode):
		result =  json.loads(result)
	#print result['kind']
	if 'List' not in result['kind']:
		return
	
	print len( result['items'] )
	if len( result['items'] ) > 0:
		for it in result['items']:
			#print it['kind']
			#print it['id']
			#print type(it['id'])
			if type(it['id'] ) in ( str, unicode ) :
				continue
			tmp_id = it['id']
			#print tmp_id['kind']
			if 'video' in tmp_id['kind']:
				print tmp_id['videoId']
				Chiama_Url_Videos(tmp_id['videoId'], access_token)
				

	print " ---------------------- FINE Parse_Result_Json ------------------ " 


def Chiama_Url( campo, access_token ):
	#https://www.googleapis.com/youtube/v3/channels
	url =  "https://www.googleapis.com/youtube/v3/" + campo 

	query_args = { 'part':'snippet',
			'mine':'true' ,
			'access_token' : access_token}
	
	data  = urllib.urlencode(query_args)


	request = urllib2.Request(url + "?" + data)

	print url + "?" + data
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	Parse_Result_Json( result_dict )


def Update_LiveBroadcast( broadcast_id, status_str, account ):


	# stati possibili : complete - live - testing ( usabile solo se  
	# contentDetails.monitorStream.enableMonitorStream property is set to true.

	result = {}
	Chiave = Json_Keys.Account_Keys['yt_account_' + account ]
	refresh_token = Chiave['ACC_TOKEN']
	print refresh_token
	access_token =  Chiama_Refresh_Accesso_Token( refresh_token )

	try:

		print " ---------------------- INIZIO Update_LiveBroadcast  ------------------ " 

		meta =  {
			'id': broadcast_id,
			'broadcastStatus': status_str
		}

		param = { 'part': 'id,contentDetails, snippet,status', 'id' : broadcast_id, 'broadcastStatus' : status_str}

		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}

		return_req = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveBroadcasts/transition',headers=headers,params=param,data=json.dumps(meta))
		
		# bind your stream

		# get youtube id
		result = json.loads(return_req.content)            
		#print " ---------------------- DEBUG Update_LiveBroadcast  ------------------ "
		print result
		#print " ---------------------- DEBUG Update_LiveBroadcast  ------------------ "
		youtube_id = result['id']
		print youtube_id
		
	except:
		pass

	print " ---------------------- FINE Update_LiveBroadcast  ------------------ " 
	return result



def Bind_LiveBroadcast( broadcast_id, stream_id, access_token ):


	result = {}

	try:

		print " ---------------------- INIZIO Bind_LiveBroadcast  ------------------ " 

		meta =  {
			'id': broadcast_id,
			'streamId': stream_id
		}

		param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}

		requset = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveBroadcasts/bind',headers=headers,params=param,data=json.dumps(meta))
		
		# bind your stream
		print requset.content

		# get youtube id
		result = json.loads(requset.content)            
		youtube_id = result['id']
		print youtube_id
		
	except:
		pass

	print " ---------------------- FINE Bind_LiveBroadcast  ------------------ " 
	return result


def Create_LiveBroadcast( title, desc, starttime, status, access_token ):

	print " ---------------------- INIZIO Create_LiveBroadcast  ------------------ " 
	'''
	mandatory:
	'snippet': {
		    'title': string,
		    'scheduledStartTime': datetime,
		  },
	'status': {
		    'privacyStatus': string
		  }
	possibili:

		meta =  {
		'snippet': {
		    'title': string,
		    'description': string,
		    'scheduledStartTime': datetime,
		    'scheduledEndTime': datetime
		  },
		  'status': {
		    'privacyStatus': string
		  },
		  'contentDetails': {
		    'monitorStream': {
		      'enableMonitorStream': boolean,
		      'broadcastStreamDelayMs': unsigned integer
		    },
		    'enableEmbed': boolean,
		    'enableDvr': boolean,
		    'enableContentEncryption': boolean,
		    'startWithSlate': boolean,
		    'recordFromStart': boolean,
		    'enableClosedCaptions': boolean
		  }
		} 
	'''
	result = {}

	try:


		meta =  {
		'snippet': {
		    'title': title,
		    'description': desc,
		    'scheduledStartTime': starttime
		  },
		  'status': {
		    'privacyStatus': status
		  }
		} 



		param = { 'part': 'snippet,status', 'access_token': access_token}


		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}

		requset = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveBroadcasts',headers=headers,params=param,data=json.dumps(meta))
		
		#upload your stream
		print requset.status_code
		print requset.content

		# get youtube id
		result = json.loads(requset.content)            
		youtube_id = result['id']
		print youtube_id
		
		print " ---------------------- FINE Create_LiveBroadcast ------------------ " 

	except:
		pass

	return result


def Upload_Video( filename, title, description, tags, category, access_token ):
	# categoryId is '1' for Film & Animation
	# to fetch all categories: https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode={2 chars region code}&key={app key}

	meta =  {'snippet': {'categoryId': '24',
	  'description': 'description',
	  'tags': ['test1', 'test2'],
	  'title': title },
	  'status': {'privacyStatus': 'public'}}

	param = { 'part': 'snippet,status',
		 'uploadType': 'resumable'}

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	#get location url
	retries = 0
	retries_count = 1
	while retries <= retries_count: 
	    requset = requests.request('POST', 'https://www.googleapis.com/upload/youtube/v3/videos',headers=headers,params=param,data=json.dumps(meta))
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	if requset.status_code != 200:
	    print 'requset.status_code != 200:'
	    #do something

	location = requset.headers['location']
	file_name = '/Users/perucccl/Downloads/VID_20160909_122114.mp4'

	file_data = open(file_name, 'rb').read()

	headers =  {'Authorization': 'Bearer ' + access_token}

	#upload your video
	retries = 0
	retries_count = 1
	while retries <= retries_count:
	    requset = requests.request('POST', location,headers=headers,data=file_data)
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	# get youtube id
	cont = json.loads(requset.content)            
	youtube_id = cont['id']
	
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 


def Chiama_Url_LiveBroadcast_Control( id, access_token ):
	#https://www.googleapis.com/youtube/v3/liveBroadcasts
	print " ---------------------- INIZIO Chiama_Url_liveBroadcasts ------------------ " 
	url =  'https://www.googleapis.com/youtube/v3/liveBroadcasts/control?part=snippet&id=' + id + '&access_token=' + access_token

	request = urllib2.Request(url)
	print url
	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	print result_dict
	print " ---------------------- FINE Chiama_Url_liveBroadcasts ------------------ " 



def Chiama_Url_Videos( id, access_token ):
	#https://www.googleapis.com/youtube/v3/videos
	print " ---------------------- INIZIO Chiama_Url_Videos ------------------ " 
	url =  'https://www.googleapis.com/youtube/v3/videos?part=snippet,id&mine=true&id=' +id +'&access_token=' + access_token

	request = urllib2.Request(url)
	print url
	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print result_dict
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 


def Chiama_Url_no_params( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json( result_dict )
	if 'stampa' in parse:
		print result_dict


def Get_LiveBroadcast_Bool( item_id, access_token ):

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	status = None
	print " ---------------------- INIZIO Get_LiveBroadcast_Status ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		if not 'status' in result['items'][0]:
			return False
		if not 'lifeCycleStatus' in result['items'][0]['status']:
			return False
		status =  result['items'][0]['status']['lifeCycleStatus']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE Get_LiveBroadcast_Status ------------------ " 
	print status

	return False




def Check_LiveBroadcast_Status( status_param, item_id, account ):


	Chiave = Json_Keys.Account_Keys['yt_account_' + account ]
	refresh_token = Chiave['ACC_TOKEN']
	print refresh_token
	access_token =  Chiama_Refresh_Accesso_Token( refresh_token )

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	status = None
	print " ---------------------- INIZIO Check_LiveBroadcast_Status ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		if not 'status' in result['items'][0]:
			return False
		if not 'lifeCycleStatus' in result['items'][0]['status']:
			return False
		status =  result['items'][0]['status']['lifeCycleStatus']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE Check_LiveBroadcast_Status ------------------ " 
	print status
	if status_param == status:
		return True

	return False


def Get_LiveBroadcast_Status( item_id, account ):


	Chiave = Json_Keys.Account_Keys['yt_account_' + account ]
	refresh_token = Chiave['ACC_TOKEN']
	print refresh_token
	access_token =  Chiama_Refresh_Accesso_Token( refresh_token )

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	status = None
	print " ---------------------- INIZIO Get_LiveBroadcast_Status ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveBroadcasts?part=status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		if not 'status' in result['items'][0]:
			return status
		if not 'lifeCycleStatus' in result['items'][0]['status']:
			return status
		status =  result['items'][0]['status']['lifeCycleStatus']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE Get_LiveBroadcast_Status ------------------ " 
	print status

	return status




def Check_LiveStream_Status( status_param, item_id, account ):

	Chiave = Json_Keys.Account_Keys['yt_account_' + account ]
	refresh_token = Chiave['ACC_TOKEN']
	print refresh_token
	access_token =  Chiama_Refresh_Accesso_Token( refresh_token )

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	status = None
	print " ---------------------- INIZIO Get_LiveStream_Status ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveStreams?part=status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		if not 'status' in result['items'][0]:
			return False
		if not 'streamStatus' in result['items'][0]['status']:
			return False
		status =  result['items'][0]['status']['streamStatus']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE Get_LiveStream_Status ------------------ " 
	print status
	if status == status_param:
		return True

	return False



def Get_LiveStream_Status( item_id, access_token ):

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	status = None
	print " ---------------------- INIZIO Get_LiveStream_Status ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveStreams?part=status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		if not 'status' in result['items'][0]:
			return status
		if not 'streamStatus' in result['items'][0]['status']:
			return status
		status =  result['items'][0]['status']['streamStatus']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE Get_LiveStream_Status ------------------ " 
	print status

	return status




def LiveStream_Event_Id_Get_Streamname( item_id, access_token , parse):

	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	stream_id = None
	print " ---------------------- INIZIO LiveStream_Event_Id_Get_Streamname ------------------ " 
	url =  "https://www.googleapis.com/youtube/v3/liveStreams?part=id,snippet,cdn,status&access_token=" + access_token + "&id=" + item_id 
	request = urllib2.Request(url)

	#print url
	#request.add_header('Authorization: Bearer ', access_token)
	#print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	#print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	result = json.loads( result_dict )

	if len(result['items'] ) > 0 :
		#print result['items'][0]['cdn']['ingestionInfo']['streamName']
		stream_id =  result['items'][0]['cdn']['ingestionInfo']['streamName']
        #print json.dumps(result, indent=4)


	print " ---------------------- FINE LiveStream_Event_Id_Get_Streamname ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json_LiveStream( result_dict )
	if 'stampa' in parse:
		print result_dict

	return stream_id



def Chiama_Url_LiveStream( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json_LiveStream( result_dict )
	if 'stampa' in parse:
		print result_dict

def Chiama_Url_LiveBroadcast( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json_LiveBroadcast( result_dict )
	if 'stampa' in parse:
		print result_dict


def Delete_LiveStream( id, access_token ):

	print " ---------------------- INIT Delete_LiveStream ------------------ " 
	# categoryId is '1' for Film & Animation
	# to fetch all categories: https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode={2 chars region code}&key={app key}
	result = {}
	try:

		meta =  {
		} 

		param = { 'id': id }


		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}

		requset = requests.request('DELETE', 'https://www.googleapis.com/youtube/v3/liveStreams',headers=headers,params=param,data=json.dumps(meta))

		
		#upload your video
		print requset.status_code
		result =  requset.content

		# get youtube id
	except:
		pass

	print " ---------------------- FINE Delete_LiveStream ------------------ " 
	return result




def Create_LiveStream( title, desc, cdn_format, cdn_ingtype, cdn_res, cdn_fps, access_token ):

	print " ---------------------- INIT Create_LiveStream ------------------ " 
	# categoryId is '1' for Film & Animation
	# to fetch all categories: https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode={2 chars region code}&key={app key}
	result = {}
	try:

		meta =  {
		  'snippet': {
		    'title': title,
		    'description': desc
		  },
		  'cdn': {
		    'format': cdn_format,
		    'ingestionType': cdn_ingtype
		    },
		    'resolution': cdn_res,
		    'frameRate': cdn_fps
		} 

		param = { 'part': 'snippet,cdn,status'}


		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}

		requset = requests.request('POST', 'https://www.googleapis.com/youtube/v3/liveStreams',headers=headers,params=param,data=json.dumps(meta))

		
		#upload your video
		print requset.status_code
		print requset.content

		# get youtube id
		result = json.loads(requset.content)            
		youtube_id = result['id']
		print youtube_id
		
	except:
		pass

	print " ---------------------- FINE Create_LiveStream ------------------ " 
	return result


def Crea_And_Bind_Livestream_Event_Old( nomestream, nomebroadcast,  starttime, privacystatus, access_token ):

	result = {}
	try :

		livebroadcast_res = Create_LiveBroadcast( nomebroadcast, 'Create_Broadcast_Description', starttime, privacystatus,  access_token)	
		if not 'id' in livebroadcast_res:
			return result

		broadcast_id = livebroadcast_res['id']


		livestreams_res = Create_LiveStream( nomestream, 'Test_Description','1080p', 'rtmp','1080p','30fps',  access_token)	
		if not 'id' in livestreams_res:
			return result

		stream_id = livestreams_res['id']
		bind_livebroadcast = Bind_LiveBroadcast(broadcast_id, stream_id,  access_token)
		result = { u'livestreams_res':livestreams_res,u'livebroadcast_res':livebroadcast_res,u'bind_livebroadcast':bind_livebroadcast}
		
	except:
		pass

	return result



def Crea_And_Bind_Livestream_Event( streamtitle, streamdesc,  starttime, privacystatus, account ):

	print ' --------------------------- INIT Crea_And_Bind_Livestream_Event ----------------------------------------' 
	result = {}
	Chiave = Json_Keys.Account_Keys['yt_account_' + account ]

	refresh_token = Chiave['ACC_TOKEN']
	print refresh_token
	access_token =  Chiama_Refresh_Accesso_Token( refresh_token )

	Get_Info_Access_Token( access_token ) 
	try :

		Get_Info_Access_Token( access_token )

		livestreams_res = Create_LiveStream( streamtitle, 'Test_Description','1080p', 'rtmp','1080p','30fps',  access_token)	
		if not 'id' in livestreams_res:
			return result

		stream_id = livestreams_res['id']

		livebroadcast_res = Create_LiveBroadcast( streamtitle, streamdesc, starttime, privacystatus,  access_token)	
		if not 'id' in livebroadcast_res:
			return result

		broadcast_id = livebroadcast_res['id']
		bind_livebroadcast = Bind_LiveBroadcast(broadcast_id, stream_id,  access_token)
		result = { u'LivestreamsDetails':livestreams_res,u'LivebroadcastDetails':livebroadcast_res,u'BindDetails':bind_livebroadcast}
		
	except:
		pass

	print ' --------------------------- FINE Crea_And_Bind_Livestream_Event ----------------------------------------' 
	return result



def wait_until(somepredicate, timeout, period=0.25, *args, **kwargs):

	mustend = time.time() + timeout
	while time.time() < mustend:
		if somepredicate(*args, **kwargs): 
			return True
		time.sleep(period)
	return False



if __name__ == "__main__":


	code='4/eZzFJhqDIwCsRQe5r8WE7QxPpjChmKLiSE9kEGhPpgo'
	'''
	da chiamare solo la prima volta per ogni code che mi arriva dall'autorizzazione interattiva
	refresh_token = Chiama_Secondo_Token( code )
	print refresh_token
	una volta ottenuto il refresh_token qui sotto partiamo sempre da quello
	'''


	refresh_token = '1/q2AG0bOHbhHkOvbtCpV6BJpxoi2bFDZ52EXnKpgUV8E'

	access_token = Chiama_Refresh_Accesso_Token( refresh_token ) 
	# per prendere le info dei canali
	#Chiama_Url('channels', access_token)
	#exit(0)
	# per prendere le info delle videocategorie
	#Chiama_Url_no_params('videoCategories?part=snippet&&regionCode=CH&access_token=' + access_token, access_token, 'niente')
	# per prendere le info dei video
	# devo passare dalla search per avere gli id su cui poi chiamare
	# la url del video
	#Chiama_Url_no_params('search?part=snippet,id&mine=true&channelId=UCv7yEZoifGFg_CioakqBMFA&maxResults=10&order=date&access_token=' + access_token, access_token, 'parsa')


	'''
	args = ()
	kwargs = { 'status_param' : 'complete', 'item_id' : '0mB5N7gtZ_o', 'account' : 'test'}
	wait_until( Check_LiveBroadcast_Status, 3, 0.25,*args, **kwargs )
	#Upload_Video('/Users/perucccl/Downloads/VID_20160909_122114.mp4','dummy', 'dummy','dummy', 'dummy', access_token) 

	#Chiama_Url_LiveStream('liveStreams?part=id,snippet,cdn,status&mine=true&access_token=' + access_token, access_token, 'stampa' )
	print ' QUI' 
	args = ()
	kwargs = { 'status_param' : 'inactive', 'item_id' : 'v7yEZoifGFg_CioakqBMFA1496673829645513', 'account' : 'test'}
	wait_until( Check_LiveStream_Status, 3, 0.25,*args, **kwargs )
	Get_LiveStream_Status('v7yEZoifGFg_CioakqBMFA1496673829645513', access_token )
	#Chiama_Url_LiveBroadcast('liveBroadcasts?part=snippet,id,contentDetails,status,statistics&mine=true&access_token=' + access_token, access_token, 'stampa' )

	Get_LiveBroadcast_Status('0mB5N7gtZ_o', 'test' )
	#Create_LiveStream('Test_Title_Live_Stream_per_clad_1.0', 'Test_Description','1080p', 'rtmp','1080p','30fps',  access_token) 

	'''


	#Chiama_Url_LiveStream('liveStreams?part=id,snippet,cdn,status&mine=true&access_token=' + access_token, access_token, 'stampa' )
	#Delete_LiveStream( 'v7yEZoifGFg_CioakqBMFA1496234980962911', access_token )
	#exit(0)


	#item_id = 'v7yEZoifGFg_CioakqBMFA1496044955404160'
	#stream_name_4_wowza = LiveStream_Event_Id_Get_Streamname( item_id , access_token, 'niente' )
	#print stream_name_4_wowza
	#exit(0)

	# questo funziona
	#Create_LiveBroadcast('Test_Title_Live_Broadcast_clad_2.0', 'Test_Description','2017-05-25T22:00:00.000Z', 'private',  access_token) 

	#Bind_LiveBroadcast('rfetRxrd6EQ', 'UCv7yEZoifGFg_CioakqBMFA',  access_token) 
	#Bind_LiveBroadcast('GJHVf30Q-hY', 'v7yEZoifGFg_CioakqBMFA1475748582948435',  access_token) 
	#Chiama_Url_LiveBroadcast('liveBroadcasts?part=snippet,id,contentDetails,status,statistics&mine=true&access_token=' + access_token, access_token, 'stampa' )

	print ' --------------------------- CREA AND BIND ----------------------------------------' 
	dict_yt_livestream = Crea_And_Bind_Livestream_Event( 'Stream_per_Test_CLAD.2.0', 'Broad_per_Test_CLAD.2.0', '2017-06-01T22:00:00.000Z', 'private', 'test' )
	print ' --------------------------- FINE CREA AND BIND ----------------------------------------'
	print dict_yt_livestream

	exit(0)

	print ' --------------------------- CREA AND BIND ----------------------------------------' 
	refresh_token = '1/q2AG0bOHbhHkOvbtCpV6BJpxoi2bFDZ52EXnKpgUV8E'
	access_token = Chiama_Refresh_Accesso_Token( refresh_token ) 
	dict_yt_livestream = Crea_And_Bind_Livestream_Event_Old( 'Stream_per_Test_CLAD.2.0', 'Broad_per_Test_CLAD.2.0', '2017-05-31T22:00:00.000Z', 'private', access_token )
	print dict_yt_livestream
	for key, value in dict_yt_livestream.iteritems():
		print key, value

	print ' fine : '

	#Chiama_Url_LiveBroadcast('liveBroadcasts?part=snippet,id,contentDetails,status,statistics&mine=true&access_token=' + access_token, access_token, 'stampa' )
	#print ' --------------------------- FINE Chiama_Url_LiveBroadcast ----------------------------------------'
	#LiveStream_Event_Get_Streamname('liveStreams?part=id,snippet,cdn,status&mine=true&access_token=' + access_token, access_token, 'stampa' )
		
