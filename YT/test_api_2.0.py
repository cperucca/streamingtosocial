#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2
import urllib
import requests
import json
import ast

'''
esempio di url per prendere il code della prima autorizzazione
https://accounts.google.com/o/oauth2/auth?client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scope=https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.upload&response_type=code&access_type=offline

ottenuto il seguente token 
code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU

_url_ = 'https://accounts.google.com/o/oauth2/auth?\
client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&\
redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&\
scope=https%3A//www.googleapis.com/auth/youtube%20https%3A//www.googleapis.com/auth/youtube.upload&\
response_type=code&\
access_type=offline'

data = urllib.urlencode( _url_ )
print data
req = urllib2.Request( _url_ )

result = urllib2.urlopen(req)
print  result.read()
# result.info() will contain the HTTP headers


e adesso devo fare una POST 

code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU
client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&
client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&
redirect_uri=http://localhost/oauth2callback&
grant_type=authorization_code


code=4/0MymML5cE6fhF-nVe081jIJkhc6j5e7jKQSFOfr6tqU&client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&redirect_uri=http://localhost/oauth2callback&grant_type=authorization_code

'''
def Chiama_Secondo_Token( code ): 

	print(' ------------------ INIZIO Chiama_Secondo_Token ----------------------')
	url = 'https://accounts.google.com/o/oauth2/token'

	values = { 'code':code,
	'client_id':'567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com',
	'client_secret':'sRLqC0TsUGlOZh3gCFlVOC6-',
	'redirect_uri':'urn:ietf:wg:oauth:2.0:oob',
	'grant_type':'authorization_code' }

	print url, values
	data = urllib.urlencode(values)

	request = urllib2.Request(url)
	
	print 'Request method before data:', request.get_method()
	request.add_data(data)
	print 'Request method after data :', request.get_method()

	print 'curl --data "' +  'code=' + code + '&client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&redirect_uri=urn:ietf:wg:oauth:2.0:oob&grant_type=authorization_code' + '" ' + url
	print '\n\n'

	print
	print 'OUTGOING DATA:'
	print request.get_data()

	print
	print 'SERVER RESPONSE:'
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	result_dict = ast.literal_eval(result_dict)
	return result_dict['refresh_token']
	print(' ------------------ FINE Chiama_Secondo_Token ----------------------')




''' ricevuto 
{
  "access_token" : "ya29.Ci9uA0EmE_yNHKaGBN6UX4hLncYM7UIvXQhLW1onPEVX6uEzcE4HqxkBssq9ZcK8MA",
  "token_type" : "Bearer",
  "expires_in" : 3600,
  "refresh_token" : "1/EQiYRrwMDa5DrhQkNEoOq23zaxss7eVOqDGZgH18G-E"
}

'''
# refreshing the access token

''' 
client_id=21302922996.apps.googleusercontent.com&
client_secret=XTHhXh1SlUNgvyWGwDk1EjXB&
refresh_token=1/6BMfW9j53gdGImsixUH6kU5RsR4zwI9lUVX-tqf8JXQ&
grant_type=refresh_token
'''

def Parse_Result_Json( result ):

	print " ---------------------- INIZIO Parse_Result_Json ------------------ " 
	if type(result) in (str, unicode):
		result =  ast.literal_eval(result)
	print result['kind']
	if 'List' not in result['kind']:
		return
	
	print len( result['items'] )
	if len( result['items'] ) > 0:
		for it in result['items']:
			#print it['kind']
			#print it['id']
			#print type(it['id'])
			if type(it['id'] ) in ( str, unicode ) :
				continue
			tmp_id = it['id']
			print tmp_id['kind']
			if 'video' in tmp_id['kind']:
				print tmp_id['videoId']
				Chiama_Url_Videos(tmp_id['videoId'], access_token)
				

	print " ---------------------- FINE Parse_Result_Json ------------------ " 


def Chiama_Refresh_Accesso_Token( refresh_token ):

	print(' ------------------ INIZIO Chiama_Refresh_Accesso_Token ----------------------')
	url = 'https://accounts.google.com/o/oauth2/token'
	print 'curl --data "' +  'client_id=567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com&client_secret=sRLqC0TsUGlOZh3gCFlVOC6-&refresh_token=1/EQiYRrwMDa5DrhQkNEoOq23zaxss7eVOqDGZgH18G-E&grant_type=refresh_token' + '" ' + url

	print(' ------------------ PRIMA CHIAMATA POST per REFRESH TOKEN ----------------------')

	query_args = { 'client_id':'567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com', 
			'client_secret':'sRLqC0TsUGlOZh3gCFlVOC6-',
			'refresh_token':refresh_token,
			'grant_type':'refresh_token' }

	request = urllib2.Request(url)
	print 'Request method before data:', request.get_method()
	request.add_data(urllib.urlencode(query_args))
	print 'Request method after data :', request.get_method()

	print
	print 'OUTGOING DATA:'
	print request.get_data()

	print
	print 'SERVER RESPONSE:'
	result =  urllib2.urlopen(request)
	result_dict =  result.read()
	print result_dict
	print(' ------------------ DOPO CHIAMATA POST per REFRESH TOKEN ----------------------')

	result_dict = ast.literal_eval(result_dict)

	print
	print 'VALID ACCESS TOKEN :'
	print result_dict['access_token']
	return result_dict['access_token']
	print(' ------------------ FINE Chiama_Refresh_Accesso_Token ----------------------')

def Chiama_Url( campo, access_token ):
	#https://www.googleapis.com/youtube/v3/channels
	url =  "https://www.googleapis.com/youtube/v3/" + campo 

	query_args = { 'part':'snippet',
			'mine':'true' ,
			'access_token' : access_token}
	
	data  = urllib.urlencode(query_args)


	request = urllib2.Request(url + "?" + data)

	print url + "?" + data
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()
#	print result_dict
	Parse_Result_Json( result_dict )

def Upload_Video( filename, title, description, tags, category, access_token ):
	# categoryId is '1' for Film & Animation
	# to fetch all categories: https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode={2 chars region code}&key={app key}

	meta =  {'snippet': {'categoryId': '24',
	  'description': 'description',
	  'tags': ['test1', 'test2'],
	  'title': 'Test_Title'},
	  'status': {'privacyStatus': 'public'}}

	param = { 'part': 'snippet,status',
		 'uploadType': 'resumable'}

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}

	#get location url
	retries = 0
	retries_count = 1
	while retries <= retries_count: 
	    requset = requests.request('POST', 'https://www.googleapis.com/upload/youtube/v3/videos',headers=headers,params=param,data=json.dumps(meta))
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	if requset.status_code != 200:
	    print 'requset.status_code != 200:'
	    #do something

	location = requset.headers['location']
	file_name = '/Users/perucccl/Downloads/VID_20160909_122114.mp4'

	file_data = open(file_name, 'rb').read()

	headers =  {'Authorization': 'Bearer ' + access_token}

	#upload your video
	retries = 0
	retries_count = 1
	while retries <= retries_count:
	    requset = requests.request('POST', location,headers=headers,data=file_data)
	    if requset.status_code in [500,503]:
		retries += 1
	    break

	# get youtube id
	cont = json.loads(requset.content)            
	youtube_id = cont['id']
	
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 



def Chiama_Url_Videos( id, access_token ):
	#https://www.googleapis.com/youtube/v3/videos
	print " ---------------------- INIZIO Chiama_Url_Videos ------------------ " 
	url =  'https://www.googleapis.com/youtube/v3/videos?part=snippet,id&mine=true&id=' +id +'&access_token=' + access_token

	request = urllib2.Request(url)
	print url
	result =  urllib2.urlopen(request)
	result_dict =  result.read()

	print result_dict
	print " ---------------------- FINE Chiama_Url_Videos ------------------ " 



def Chiama_Url_no_params( campo, access_token , parse):
	#https://www.googleapis.com/youtube/v3/ campo_parametrizzato
	url =  "https://www.googleapis.com/youtube/v3/" + campo 
	request = urllib2.Request(url)

	print " ---------------------- INIZIO Chiama_Url_no_params ------------------ " 
	print url
	#request.add_header('Authorization: Bearer ', access_token)
	print 'Request method after add_header :', request.get_method()

	result =  urllib2.urlopen(request)
	result_dict =  result.read()

#	print result_dict
	print " ---------------------- FINE Chiama_Url_no_params ------------------ " 
	if 'parsa' in parse:
		Parse_Result_Json( result_dict )
	if 'stampa' in parse:
		print result_dict



code='4/eZzFJhqDIwCsRQe5r8WE7QxPpjChmKLiSE9kEGhPpgo'
'''
da chiamare solo la prima volta per ogni code che mi arriva dall'autorizzazione interattiva
refresh_token = Chiama_Secondo_Token( code )
una volta ottenuto il refresh_token qui sotto partiamo sempre da quello
'''
refresh_token = '1/q2AG0bOHbhHkOvbtCpV6BJpxoi2bFDZ52EXnKpgUV8E'
access_token = Chiama_Refresh_Accesso_Token( refresh_token ) 
# per prendere le info dei canali
Chiama_Url('channels', access_token)
# per prendere le info delle videocategorie
Chiama_Url_no_params('videoCategories?part=snippet&&regionCode=CH&access_token=' + access_token, access_token, 'niente')
# per prendere le info dei video
# devo passare dalla search per avere gli id su cui poi chiamare
# la url del video
Chiama_Url_no_params('search?part=snippet,id&mine=true&channelId=UCv7yEZoifGFg_CioakqBMFA&maxResults=10&order=date&access_token=' + access_token, access_token, 'parsa')
#Upload_Video('/Users/perucccl/Downloads/VID_20160909_122114.mp4','dummy', 'dummy','dummy', 'dummy', access_token) 



