#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib2
import urllib   
import urlparse
import os, sys, random, string, argparse

from tornado import httpclient
import tornado.ioloop
import tornado.web

YT_CLIENT_ID = "567286830981-2r1sg7h1d6alu7kj4lbtbr6flnbdqbqk.apps.googleusercontent.com"
YT_CLIENT_SECRET = 'sRLqC0TsUGlOZh3gCFlVOC6-'


class ytOauthHandler(tornado.web.RequestHandler):
    def get(self):
        http_client = httpclient.AsyncHTTPClient()
        code = self.request.arguments['code'][0]
        post_data = {   'code'          : 4/nejM5H5JPwN0JWJv56Zg9LJP1nhXiVubIwd6btbryrI, 
                        'client_id'     : YT_CLIENT_ID, 
                        'client_secret' : YT_CLIENT_SECRET, 
                        'redirect_uri'  : 'http://localhost:8888/oauth_callback/', 
                        'grant_type'    : 'authorization_code',
			}         

        body = urllib.urlencode(post_data)                          
        url = "https://accounts.google.com/o/oauth2/token"
        http_client.fetch(url, self.handle_request, method='POST', headers={'Content-Type' : 'application/x-www-form-urlencoded'}, body=body) #Send it off!
        self.write('OK')

    def handle_request(self, response):
        print response


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, MainHandler")
        pass


application = tornado.web.Application([
    (r"/oauth_callback/", ytOauthHandler),
    (r"/", MainHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()


