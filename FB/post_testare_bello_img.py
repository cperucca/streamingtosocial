import facebook
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

def main():
  # Fill in the values noted in previous steps here
  cfg = {
    "page_id"      : "1237091532989230",  # Step 1
    "access_token" : "EAAEfhg8OyOkBAHRHZCfPcyV2XGcFEUZBdYWRz9kZAfgkDIxh91PY8NDWORIUKdyeEVzqr22H4KHRUYprC7K5vWmwCLAz7iPeHQZBXw6o88SaDqfQyZCBHWdxRmQOMrgSznMwN24klOuleL8nN57LESJdmDZC8StQoZD"   # Step 3
    }

  api = get_api(cfg)


  print api.get_connections(id='me', connection_name='')
  posts =  api.get_connections(id='me', connection_name='posts')
  print posts
  print posts['data']
  for dat in posts['data']:
	print ' --- ' + dat['id'] + " --- "
	for k, v in dat.iteritems():
	    print k, v
	print 'object da id = ' + str(api.get_object(id=dat['id']))
	print api.get_connections(id=dat['id'], connection_name='')
  msg = "Hello, world!"
  status = api.put_wall_post(msg)

def get_api(cfg):
  graph = facebook.GraphAPI(cfg['access_token'])
  # Get page token to post as the page. You can skip 
  # the following if you want to post as yourself. 
  resp = graph.get_object('me/accounts')
  page_access_token = None
  for page in resp['data']:
    if page['id'] == cfg['page_id']:
      page_access_token = page['access_token']
  graph = facebook.GraphAPI(page_access_token)
  return graph
  # You can also skip the above if you get a page token:
  # http://stackoverflow.com/questions/8231877/facebook-access-token-for-pages
  # and make that long-lived token as in Step 3

if __name__ == "__main__":
  main()

