#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Keys.Json_Keys as Json_Keys

import codecs
import urllib2
import urllib
import requests
import json
import ast
import logging
from datetime import datetime, timedelta
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def Get_Tag_List( tag  ):

        print " ---------------------- INIZIO Get_Tag_List  ------------------ "

	print tag
	
	url_per_info = 'https://graph.facebook.com/v2.8/search?type=adinterest'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD',
                'access_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        meta =  {
                'page_id': '1237091532989230',
                'access_token': '316135615416553|Fo6rae5J89_MlwVETXO4sQGZ4wA'
        }
        param =  {
                'page_id': '1237091532989230',
		'q':tag,
                'access_token': '316135615416553|Fo6rae5J89_MlwVETXO4sQGZ4wA'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Tag_List  ------------------ "
        return



def Get_Page_Permissions(  ):

        print " ---------------------- INIZIO Page Permissions  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/250555604987264/permissions'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD',
                'access_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        meta =  {
                'page_id': '250555604987264',
                'access_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD'
        }
        param =  {
                'page_id': '250555604987264',
                'access_token': 'EAAEfhg8OyOkBACez34jePnaD7NmtoXKFtuKujbfhnCnjvoAFLCWH691scarGkYpCRBIFG2MwNXi3APsgq6ULudBQZBscYFBTRDgHvrR4G0B1NuZCNMNpv0jvZCWKp1DLMYqiZAZB3fTxXGDy0Bb4ZAzoZC8ELSqK5YZD'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Page Permissions  ------------------ "
        return


def Get_App_Token( ):

        print " ---------------------- INIZIO Get_Long_Token  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/me/accounts'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD',
                'access_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {

                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
}
        param = {}
        param =  {
                'client_id': '316135615416553',
                'client_secret': '10d60db7adf828e0f696f886455c99b0',
                'grant_type': 'client_credentials',
                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Long_Token  ------------------ "
        return result


def Get_Token_Param( token ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/debug_token'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD',
                'access_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'input_token': token,
                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return

def Get_Token_Info( ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/debug_token'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return


def Get_IA_Info_All( ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com/1237091532989230/instant_articles
	url_per_info = 'https://graph.facebook.com/v2.8/1237091532989230/instant_articles'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return


def Get_IA_Info_From_Canonical( url ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com?id={canonical-url}&amp;fields=instant_article
	url_per_info = 'https://graph.facebook.com/v2.8/'

        meta = {}
        param = {}
        param =  {
		'id' : url,
		'fields' : 'instant_article',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return

def Get_IA_Detail( id ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com/{article-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id

        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return



def Get_Live_Videos( account ):

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	# https://graph.facebook.com/{page-id}/live_videos
	page_id = Chiave['PAGE_ID']
	url_per_info = 'https://graph.facebook.com/v2.8/' + page_id + '/live_videos'

        meta = {}
        param = {}
        param =  {
                'access_token': Chiave['ACC_TOKEN']
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Live_Video  ------------------ "
        return

def Get_Live_Video_Preview_Url( id, account ):

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	# https://graph.facebook.com/{video-id}
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	url_per_info = 'https://graph.facebook.com/v2.8/' + id

        meta = {}
        param = {}
        param =  {
                'access_token': Chiave['ACC_TOKEN'],
		'fields' : 'dash_preview_url '
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Live_Video  ------------------ "
        return

def Get_Live_Video_Details( id , account):

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	result = {}
	
	try :
		# https://graph.facebook.com/{video-id}
		url_per_info = 'https://graph.facebook.com/v2.8/' + id

		meta = {}
		param = {}
		param =  {
			'access_token': Chiave['ACC_TOKEN'],
			'fields' : 'title,planned_start_time,status,is_manual_mode,dash_preview_url,broadcast_start_time,creation_time,description,embed_html,live_views,permalink_url,seconds_left,secure_stream_url'


		}
		headers =  { 'Accept': 'application/json'}

		requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

		# bind your stream
		print '\r\n'
		result = json.loads( requ.text )
		#print json.dumps(result, indent=4)
		print " ---------------------- FINE Get_Live_Video  ------------------ "
	except:
		pass

	return result


def Update_Live_Video_Detail_List( id, list_details, account ):

        print " ----------------------pyFacebookClass INIZIO Update_Live_Video  ------------------ "
	result = {}
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	
	# https://graph.facebook.com/{video-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id + '?'   + list_details

        meta = {}
        param = {}
        param =  {
                'access_token': Chiave['ACC_TOKEN'],
		'fields' : 'title,planned_start_time,status,is_manual_mode,dash_preview_url,broadcast_start_time,creation_time,description,embed_html,live_views,permalink_url,seconds_left,secure_stream_url'


        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ----------------------pyFacebookClass FINE Update_Live_Video  ------------------ "
        return result



def Update_Live_Video_Details( id, key, value, account ):

        print " ---------------------- INIZIO Update_Live_Video  ------------------ "
	result = {}

	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	
	# https://graph.facebook.com/{video-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id + '?' + key + '=' + value 

        meta = {}
        param = {}
        param =  {
                'access_token': Chiave['ACC_TOKEN'],
		#'fields' : 'title,planned_start_time,status,is_manual_mode,dash_preview_url,broadcast_start_time,creation_time,description,embed_html,live_views,motion_detection_settings,permalink_url,seconds_left,secure_stream_url'
		# modificato per vedere solo status e planned_time in risposta alla update
		'fields' : 'planned_start_time,status'


        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Update_Live_Video  ------------------ "
        return result

def Create_Scheduled_Video( title, description, schedule_time , account ):


        print " ----------------------pyFACEBOOKClass INIZIO Create Scheduled_Video()------------------ "
	result = {}
	Chiave = Json_Keys.Account_Keys['fb_account_' + account ]
	
	try:
	
		# https://graph.facebook.com/{page-id}/live_videos
		# pero deve essere fatto dalla app 

		page_id = '283344488362396'
		page_id = '1237091532989230'
		page_id = Chiave['PAGE_ID']
		print 'Page_Id = ' + page_id
		url_per_info = 'https://graph.facebook.com/v2.10/' + page_id + '/live_videos'

		access_token = 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
		access_token = 'EAACEdEose0cBAFWwPJT3jlipRZBOVZArS8jgBcUudQoWBzxPqZAUGC7hfPQ0RNHzZAuIdV5xvTXtSXu9gRK7MIjkdOXhqDt4ORBLsLzGPzMmC94dIMaBVDtNWYcZCvasxCANJkIP3Ukqmm0HDxpW3yiclIl7yTeSlGBNZCqZC7B6tQ9l4rErfbaFvjhToiHvWkZD'
		access_token = 'EAAEfhg8OyOkBACVFLAMqAQsyQPo50I9HVD6klbjiS64T4UCz7u5qFWp8v6Qg2T86SwSYviHXLrYAfhaTXXkLrIgZA2c63rxrF9ZCAmIeI1oHbmoNaQWCZCTr0clakQ0hfzATjud2G07BSd8hb2WenYV21QPdHAZD'
		access_token = 'EAAEfhg8OyOkBAJRQI8zBPfAxILeGWDxpCALm1VrvIbgEGcK9xZAUHZArdE7VpwPSqlwaE13igtxMqyHJ1KAIjMStl8ZCGX7TjBgTw1sTBDRGg6HdhECGqZBwVa2fY2RN9QPDhA0rUqmNzWVSN9G4FGyIuYU5gFiyEJ8sE3PjlYptBpvwsTuw'
		access_token = Chiave['ACC_TOKEN']
		meta = {}
		param =  {
			'access_token': access_token,
			'planned_start_time' : schedule_time
		}

		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}
		requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

		print '\r\n'
		video_dictionary = json.loads( requ.text )
		print json.dumps(video_dictionary, indent=4)
		
		if 'secure_stream_url' in video_dictionary:
			print ' -------------- con update -------------------'
			print title
			update_list = "title=" + title 
			print description
			update_list = update_list + "&description=" + description
			#update_list = update_list + "&planned_start_time=" + description
			#update_list = update_list + "&save_vod=false"
			result =  Update_Live_Video_Detail_List( video_dictionary['id'], update_list, account )
			
			print " ----------------------pyFACEBOOKClass  FINE Create Live_Video  ------------------ "
			print result
		else:
			result = video_dictionary
		
	except:
		pass
        return result



def Create_Live_Video( title, description, account):


        print " ---------------------- INIZIO Create Live_Video()------------------ "
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	result = {}
	
	try:
	
		# https://graph.facebook.com/{page-id}/live_videos
		# pero deve essere fatto dalla app 

		page_id = Chiave['PAGE_ID']
		url_per_info = 'https://graph.facebook.com/v2.8/' + page_id + '/live_videos'

		access_token = Chiave['ACC_TOKEN']
		meta = {}
		param =  {
			'access_token': access_token,
			'description': description,
			'save_vod' : 'false',
			'title' : 'TITOLONE',
			'status' : 'UNPUBLISHED',
			'title' : title,
			'fields' : 'secure_stream_url'
		}

		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}
		requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

		print '\r\n'
		result = json.loads( requ.text )
		print json.dumps(result, indent=4)
		print ' -------------- con dettagli -------------------'
		result.update( Get_Live_Video_Details( result['id'] , account ) )
		print " ---------------------- FINE Create Live_Video  ------------------ "
	except:
		pass
        return result


def Delete_Live_Video( video_id , account):


        print " ---------------------- INIZIO Delete Live_Video()------------------ "
	Chiave = Json_Keys.Account_Keys[ 'fb_account_' + account ]
	result = {}
	
	try:
	
		# https://graph.facebook.com/{page-id}/live_videos
		# pero deve essere fatto dalla app 

		page_id = Chiave['PAGE_ID']
		url_per_info = 'https://graph.facebook.com/v2.8/' + video_id 
		access_token = Chiave['ACC_TOKEN']
		meta = {}
		param =  {
			'access_token': access_token,
		}

		headers =  {'Authorization': 'Bearer ' + access_token,
			   'Content-type': 'application/json'}
		requ = requests.request('DELETE', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

		print '\r\n'
		result = json.loads( requ.text )
		print json.dumps(result, indent=4)
	except:
		pass
        return result

def Create_Scheduled_Live_Video( titolo, descrizione, start_time, account ):


        print " ---------------------- INIZIO  Create_Scheduled_Live_Video()------------------ "
	# per creare lo scheduled ci vuole un planned_start_time
	# che per adesso ricevo dall xls in forma 10.01.2017  19:00:00
	# e che deve essere passata come unixTime
	# ed eventualmente una scheduled_custom_profile_image
	
	# https://graph.facebook.com/{page-id}/live_videos
	Chiave = Json_Keys.Account_Keys['fb_account_' + account ]

	page_id = '1237091532989230'
	page_id = Chiave['PAGE_ID']
	url_per_info = 'https://graph.facebook.com/v2.10/' + page_id + '/live_videos'

	access_token = 'EAAEfhg8OyOkBACVFLAMqAQsyQPo50I9HVD6klbjiS64T4UCz7u5qFWp8v6Qg2T86SwSYviHXLrYAfhaTXXkLrIgZA2c63rxrF9ZCAmIeI1oHbmoNaQWCZCTr0clakQ0hfzATjud2G07BSd8hb2WenYV21QPdHAZD'
	access_token = Chiave['ACC_TOKEN']
        meta = {}
        param =  {
                'access_token': access_token,
		'title' : titolo,
		'description': descrizione,
		'save_vod' : 'false',
		'planned_start_time'  : start_time,
        }

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}
        requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        print '\r\n'
        video = json.loads( requ.text )
        print json.dumps(video, indent=4)


	update_list = "status=SCHEDULED_LIVE"
	print update_list
	result = Update_Live_Video_Detail_List( video['id'], update_list, account )
	
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Create_Scheduled_Live_Video  ------------------ "
        return result

def Scrivi_Items_Db( db_name, lista ):
        fout = codecs.open( db_name, 'w', 'utf-8')
        json.dump( lista, fout )
        fout.close()


def Prendi_Items_Db( db_name ):

        result_list = []
	try :
		print 'leggo DB_NAME ' + db_name
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_list = json.load( fin )
		print result_list
		fin.close()
	except:
		print 'PROBLEMI CON IL DB'
		pass

        return result_list



if __name__ == "__main__":

	#print Delete_Live_Video( '1600088586689521', 'testare_e_bello' )
	#print Delete_Live_Video( '1521803881184659', 'testare_e_bello' )
	#print Delete_Live_Video( '1521803624518018', 'testare_e_bello' )

	#exit(0)
	'''
	Get_Page_Permissions( )
	exit(0)


	Get_Tag_List('Albert Einstein')
	Get_Tag_List( 'Einstein')
	Get_Tag_List( 'Einstein Albert')
	Get_Tag_List('albert einstein')
	Get_Tag_List('claudio perucca')
	exit(0)
	'''

	tempo = datetime.utcnow() + timedelta( hours = 2 )  + timedelta( minutes = 55 ) # con timedelta + 2 = adesso
	tempo = '2017-08-29T13:35:00'
	tempo = datetime.strptime( tempo, '%Y-%m-%dT%H:%M:%S') # con timedelta + 2 = adesso
	print tempo
	print tempo.strftime("%s")
	exit(0)
	video_dictionary = Create_Scheduled_Video( 'TITOLO_TEST_2', 'DESCRIZIONE_TEST_2', tempo.strftime("%s"), 'testare_e_bello' )
	exit(0)
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')

	exit(0)
	print Update_Live_Video_Details( video_dictionary['id'], "status", "LIVE_NOW", 'testare_e_bello' )
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')
	exit(0)



	#Get_App_Token()
	#Get_Token_Info( )
	#Get_IA_Info_All( )
	#Get_IA_Info_From_Canonical( 'http://www.rsi.ch/sport/hockey/Cerchiamo-di-migliorare-ogni-giorno-8812618.html' )
	#Get_IA_Detail( '265308860562799' )
	#Get_Page_Permissions( )
	#video_dictionary =  Create_Live_Video()
	#print video_dictionary
	#exit(0)
	#print 'Get_Live_Videos : ' + str(Get_Live_Videos())
	#print 
	#print 
	#print Get_Live_Video_Details( '1424281904270191' )
	#print Get_Live_Video_Details( '1424293257602389' )
	#print Get_Live_Video_Details( '1432343936797321' )
	'''
	
	#print Update_Live_Video_Details( '1472139886151059', "end_live_video", "true" )
	#print Update_Live_Video_Details( '1472366252795089', "status", "LIVE_NOW" )
	#print Get_Live_Video_Details( '1473203312711383' )
	#exit(0)
	#print Delete_Live_Video( '1473370502694664' )
	#update_list = "planned_start_time=" +  "1493217240"  + "&status=SCHEDULED_LIVE"
	#print Update_Live_Video_Detail_List( '1473535642678150', update_list )
	#print Get_Live_Video_Details( '1474561502575564' )
	#print Delete_Live_Video( '1474566079241773' )
	#exit(0)
	'''
	#print Update_Live_Video_Details( '1467568169941564', "status", "LIVE_NOW" )
	#print Get_Live_Video_Details( '1467568169941564' )
	#exit(0)
	
	'''
	#print Update_Live_Video_Details( '1453871091311272', "description", "CLAD Description Updatata da API" )
	#print Update_Live_Video_Details( '1453871091311272', "title", "CLAD Titolo Updatato da API" )
	#print Update_Live_Video_Details( '1453871091311272', "status", "LIVE_NOW" )
	''' 
	''' 
	#video_dictionary =  Create_Live_Video('PARAM_TITLE', 'PARAM_DESC')
	#print video_dictionary
	#exit(0)
	#print Get_Live_Video_Details( video_dictionary['id'] )
	#tempo = datetime.utcnow() + timedelta( hours = 2 )  + timedelta( minutes = 15 ) # con timedelta + 2 = adesso
	#print tempo
	#print tempo.strftime("%s")

	#video_id = '1474667885898259'
	#print Get_Live_Video_Details( video_id )
	#exit(0)

	#update_list = "status=LIVE_NOW"
	#update_list = update_list + "&description=Generic RTMP Description Updatata da API" 
	#update_list = update_list + "&title=Generic RTMP TITOLO Updatata da API" 
	#print Get_Live_Video_Details( video_id )
	#exit(0)
	''' 
	update_list = "end_live_video=true"
	print Update_Live_Video_Detail_List( '1495340207164360', update_list , 'testare_e_bello' )

	#print Delete_Live_Video( '1495340207164360', 'testare_e_bello' )
	#exit(0)

	tempo = datetime.utcnow() + timedelta( hours = 2 )  + timedelta( minutes = 55 ) # con timedelta + 2 = adesso
	tempo = '2017-05-21T16:20:12'
	tempo = datetime.strptime( tempo, '%Y-%m-%dT%H:%M:%S') # con timedelta + 2 = adesso
	print tempo
	print tempo.strftime("%s")
	#exit(0)
	video_dictionary = Create_Scheduled_Video( 'TITOLO_TEST_2', 'DESCRIZIONE_TEST_2', tempo.strftime("%s"), 'testare_e_bello' )
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')
	print Update_Live_Video_Details( video_dictionary['id'], "status", "LIVE_NOW", 'testare_e_bello' )
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')
	exit(0)

	video_dictionary =  Create_Live_Video('TITOLO', tempo, 'testare_e_bello' ) 
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')
	update_list = "planned_start_time=" +  tempo.strftime("%s")
	#update_list = update_list + "&status=UNPUBLISHED"
	#update_list = update_list + "&description=UNPUBLISHED Description Updatata da API" 
	#update_list = update_list + "&title=UNPUBLISHED TITOLO Updatata da API" 
	print Update_Live_Video_Detail_List( video_dictionary['id'], update_list , 'testare_e_bello')
	print Get_Live_Video_Details( video_dictionary['id'] , 'testare_e_bello')
	exit(0)
	
	#print Update_Live_Video_Details( video_dictionary['id'], "planned_start_time", tempo.strftime("%s") )
	#print Update_Live_Video_Details( video_dictionary['id'], "status", "SCHEDULED_LIVE" )
	#print Update_Live_Video_Details( video_dictionary['id'], "is_manual_mode", "" )
	#print Update_Live_Video_Details( '1467429476622100', "end_live_video", "true" )
	#print Get_Live_Video_Details( video_dictionary['id'] )

	#print Get_Live_Video_Preview_Url( '1432343936797321' )
	#tempo = datetime.utcnow() + timedelta( hours = 1 )
	#print tempo
	#print tempo.strftime("%s")
	#Create_Scheduled_Live_Video( tempo.strftime("%s") )
	

	
