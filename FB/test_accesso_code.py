#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import urllib2
import urllib
import requests
import json
import ast
import logging
from datetime import datetime, timedelta
requests.packages.urllib3.disable_warnings()

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True



def Get_Page_Permissions( ):

        print " ---------------------- INIZIO Page Permissions  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/316135615416553/permissions'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD',
                'access_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        meta =  {
                'page_id': '1237091532989230',
                'access_token': '316135615416553|Fo6rae5J89_MlwVETXO4sQGZ4wA'
        }
        param =  {
                'page_id': '1237091532989230',
                'access_token': '316135615416553|Fo6rae5J89_MlwVETXO4sQGZ4wA'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Page Permissions  ------------------ "
        return


def Get_App_Token( ):

        print " ---------------------- INIZIO Get_Long_Token  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/me/accounts'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD',
                'access_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {

                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
}
        param = {}
        param =  {
                'client_id': '316135615416553',
                'client_secret': '10d60db7adf828e0f696f886455c99b0',
                'grant_type': 'client_credentials',
                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Long_Token  ------------------ "
        return result


def Get_Token_Param( token ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/debug_token'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD',
                'access_token': 'EAAEfhg8OyOkBAI0tOOjefFbvVWGcyTja3KT87O4AZCCVPvZBc5piouoBCAmZA3iDRJcNZCAAqH1mnZAPylhga6qFdecdZBZArZB7evwqh4c6RGpZAbhHTS9Yd6qeDLEp2q66u4ZCLhedi3MRHNw0i6HQSby7vO7bk1VRsZD'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'input_token': token,
                'access_token': 'EAAEfhg8OyOkBAEOYorySy4AXWYZCj8eZCxlTkcVynwGNdLRuHzARNVUoZB16ZA0kIc4KMY5k6deEsX614bDf5HSJbtwZB5BN4I3sR7m0ZAE6cVYULYyN7BeD83EzXm8R65zOuSS7GVRP9b90GHCA7l'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return

def Get_Token_Info( ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	url_per_info = 'https://graph.facebook.com/v2.8/debug_token'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return


def Get_IA_Info_All( ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com/1237091532989230/instant_articles
	url_per_info = 'https://graph.facebook.com/v2.8/1237091532989230/instant_articles'

        '''
        meta =  {
                'input_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }

        param = { 'part': 'id,contentDetails', 'id' : broadcast_id, 'streamId' : stream_id}

        '''
        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return


def Get_IA_Info_From_Canonical( url ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com?id={canonical-url}&amp;fields=instant_article
	url_per_info = 'https://graph.facebook.com/v2.8/'

        meta = {}
        param = {}
        param =  {
		'id' : url,
		'fields' : 'instant_article',
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return

def Get_IA_Detail( id ):

        print " ---------------------- INIZIO Info  ------------------ "
	
	# https://graph.facebook.com/{article-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id

        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Info  ------------------ "
        return



def Get_Live_Videos():

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	# https://graph.facebook.com/{page-id}/live_videos
	page_id = '1237091532989230'
	url_per_info = 'https://graph.facebook.com/v2.8/' + page_id + '/live_videos'

        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO'
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Live_Video  ------------------ "
        return

def Get_Live_Video_Preview_Url( id ):

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	# https://graph.facebook.com/{video-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id

        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
		'fields' : 'preview_url '
        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Live_Video  ------------------ "
        return



def Get_Live_Video_Details( id ):

        print " ---------------------- INIZIO Get_Live_Video  ------------------ "
	
	# https://graph.facebook.com/{video-id}
	url_per_info = 'https://graph.facebook.com/v2.8/' + id

        meta = {}
        param = {}
        param =  {
                'access_token': 'EAAEfhg8OyOkBALJUq1W78OyMT0qtJSBxQhGatFsIQIhPqSx2jvgaZA69oH2D1q861xJUK7gQzPyKOcZClJljSQZCO5tCNUZAZBqPkGMXyjrnwvVQhVb8J0OqWZAO9L5wAN0etPR2Ji2ABUqSaT3oLO',
		'fields' : 'title,planned_start_time,status,is_manual_mode,preview_url,broadcast_start_time,creation_time,description,embed_html,live_views,motion_detection_settings,permalink_url,seconds_left,secure_stream_url'


        }
        headers =  { 'Accept': 'application/json'}

        requ = requests.request('GET', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        # bind your stream
        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Get_Live_Video  ------------------ "
        return

def Create_Live_Video():


        print " ---------------------- INIZIO Create Live_Video()------------------ "
	
	# https://graph.facebook.com/{page-id}/live_videos
	page_id = '1237091532989230'
	url_per_info = 'https://graph.facebook.com/v2.8/' + page_id + '/live_videos'

	access_token = 'EAAEfhg8OyOkBADXONxZC2mDTkSa0lqp7ddBSMNrRtXz6PanjZARr3rVeIN6PoY94UFKP1rW3APdnvA7ZAC6Ih7JXSfabmrZAKVO4nFQiQxiLKZAmlIJZAlmfh8UzQFvFxQi6GvRGrcGj7fkUGLoKZApBJs3NdxU9hVyRDwdSA3gduczgIdedmYPTdtJSpPiBZB8ZD'
        meta = {}
        param =  {
                'access_token': access_token,
		'description': 'Testo di descrizione',
		'save_vod' : 'false',
		'status' : 'UNPUBLISHED',
		'title' : 'Titolo dello Stream da API',
		'fields' : 'preview_url '
        }

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}
        requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Create Live_Video  ------------------ "
        return

def Create_Scheduled_Live_Video( start_time ):


        print " ---------------------- INIZIO  Create_Scheduled_Live_Video()------------------ "
	# per creare lo scheduled ci vuole un planned_start_time
	# che per adesso ricevo dall xls in forma 10.01.2017  19:00:00
	# e che deve essere passata come unixTime
	# ed eventualmente una scheduled_custom_profile_image
	
	# https://graph.facebook.com/{page-id}/live_videos
	page_id = '1237091532989230'
	url_per_info = 'https://graph.facebook.com/v2.8/' + page_id + '/live_videos'

	app_access_token = 'EAAEfhg8OyOkBAKnlocp3NvdZAp3OyZCZCuJO4RNCeuMOXwQ3yS6Jfd7NHWKdhFVf60MUTHrrnOdkT5cBL8KT2t24PbZAYzd3jgRW2AFdOib0TLd7GiCNAMnVmfhTBmuDJc4ZA4GnC5w4PhAgDw7iZA'
	page_access_token = 'EAAEfhg8OyOkBAMdAPh27zTmx4yPThFmoa5HX9nYFFX26weVQq1P0J6a63LLOdCkWZC6rnIeOepbu3V4gXiYOP63g5uw5tBD1w6VlFMxUAZAVS4CPbEyHetMO7sDjjynjB90eQLvvZA8FPqayo99MfkdwMbJvaJuu1WLSaDnPAZDZD'
	access_token = page_access_token
        meta = {}
        param =  {
                'access_token': access_token,
		'title' : u'Titolo dello Stream da API',
		'description': u'Testo di descrizione',
		'planned_start_time': start_time,
		'save_vod' : 'false',
		'status' : 'UNPUBLISHED',
		'fields' : 'preview_url '
        }

	headers =  {'Authorization': 'Bearer ' + access_token,
		   'Content-type': 'application/json'}
        requ = requests.request('POST', url_per_info ,headers=headers,params=param,data=json.dumps(meta))

        print '\r\n'
        result = json.loads( requ.text )
        print json.dumps(result, indent=4)
        print " ---------------------- FINE Create_Scheduled_Live_Video  ------------------ "
        return


if __name__ == "__main__":
	#Get_App_Token()
	Get_Token_Info( )
	#Get_IA_Info_All( )
	#Get_IA_Info_From_Canonical( 'http://www.rsi.ch/sport/hockey/Cerchiamo-di-migliorare-ogni-giorno-8812618.html' )
	#Get_IA_Detail( '265308860562799' )
	#Get_Page_Permissions( )
	#print Create_Live_Video()
	#print 'Get_Live_Videos : ' + str(Get_Live_Videos())
	#print 
	#print 
	#print Get_Live_Video_Details( '1424281904270191' )
	#print Get_Live_Video_Details( '1424293257602389' )
	#print Get_Live_Video_Details( '1424338517597863' )
	#print Get_Live_Video_Preview_Url( '1424338517597863' )
	#tempo = datetime.utcnow() + timedelta( hours = 1 )
	#print tempo
	#print tempo.strftime("%s")
	#Create_Scheduled_Live_Video( tempo.strftime("%s") )
	

	
