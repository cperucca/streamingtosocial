
# -*- coding: utf-8 -*-

import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }


for entry in namespaces:
	#print entry, namespaces[entry]
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry

def Aggiungi_Items_al_Db( archive_name, archive_dict ):
	
	if len(archive_dict) == 0 :
		return
        old_dict = {}
	if os.path.isfile( archive_name ):
		adesso = datetime.utcnow()
		shutil.copy( archive_name, archive_name + "_" + adesso.strftime("%s") )
		try :
			print 'AGGIUNGO a  ARCHIVE_NAME ' + archive_name
			fin = codecs.open( archive_name, 'r', 'utf-8' )
			old_dict = json.load( fin )
			print old_dict
			fin.close()
		except:
			print 'PROBLEMI CON IL DB'
			pass


	for key,value in archive_dict.iteritems():
		old_dict[ key ] = value


        fout = codecs.open( archive_name, 'w', 'utf-8')
        json.dump( old_dict, fout )
        fout.close()



def Scrivi_Items_Db( db_name, lista ):

	
	
	#if len(lista) == 0 :
		#return

	#adesso = datetime.utcnow()

	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )

        fout = codecs.open( db_name, 'w', 'utf-8')
        json.dump( lista, fout )
        fout.close()

def Prendi_Items_Db( db_name ):

        result_dict = {}
	try :
		#print 'leggo DB_NAME ' + db_name
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_dict = json.load( fin )
		#print result_dict
		fin.close()
	except:
		print 'PROBLEMI CON IL DB'
		pass

        return result_dict

def Aggiungi_Items_da_Db( db_name, dict_content):

	# deve aggiungere tutti e solo gli item che ci non nel Db e che mancano
	# alla query del Solr cioe non sono stati modificati nell'ultimo minuto

	#print dict_content
	result = dict_content

	for key,value in dict_content.iteritems():
		dict_content[ key ]['FBStreamingDetails'] = {}
		dict_content[ key ]['YTStreamingDetails'] = {}
	
	# quindi prendo quelli del db
	dict_db = Prendi_Items_Db( db_name )
	print '  totale content preso dal DB        =  ' + str(len(dict_db))
	# e verifico che non siano gia' presenti nella dict_content
	for key, value in dict_db.iteritems():
		#print key
		#print value
		#print 
		#print value['SocialFlags']
		#print value['ECEDetails']
		
		_appendo = True
		if key in dict_content:
			# questo non lo aggiungo
			_appendo = False
			# se esistono gia i video details allora li copia nelle nuove info che arrivano da ECE
			# magari qualcuno ha cambiato qualche valore di time e dobbiamo riverificare tutto
			# in base a quell id ma non perdere i videodetail e neppure i wowza details
			if 'FBStreamingDetails' in value:
				dict_content[ key ]['FBStreamingDetails'] = value['FBStreamingDetails']
			if 'YTStreamingDetails' in value:
				dict_content[ key ]['YTStreamingDetails'] = value['YTStreamingDetails']
					
		if _appendo:
			result[ key ] = value
	
	return result

def printList( dict_content ):


	for key, value in dict_content.iteritems():
		print ' Key -----> ' + key
		if 'ECEDetails' in value :
			print '----  ECEDetails : '	
			print value['ECEDetails']
		if 'SocialFlags' in value :
			print '----  SocialFlags : '	
			print value['SocialFlags']
		if 'FBStreamingDetails' in value and len(value['FBStreamingDetails']) > 0:
			print '----  FBStreamingDetails : '	
			for keystream,valuestrem in value['FBStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		if 'YTStreamingDetails' in value and len(value['YTStreamingDetails']) > 0:
			print '----  YTStreamingDetails : '	
			for keystream,valuestrem in value['YTStreamingDetails'].iteritems():
				print 'Details of : ' + keystream
				print valuestrem
		print '------------------------- fine ' + key + '-----------------------'
	


if __name__ == "__main__":

	#Get_Item_Content('8845033')
	#exit(0)

	#Put_Id( 8732965, './_test_cambiamento_')
	#exit(0)

	#print Prendi_Social_TimeCtrl('8730301')
	# print Prendi_Social_TimeCtrl('8568391')
	
	#print Check_Data_Range( datetime.utcnow(), '8568391')
	#print 'Adesso CLAD'
	#print Prendi_Altro_Content( '8730301')
	#exit(0)

	#lista =  Prendi_Flag_Social( 8730301 )
	#for lis in lista.iteritems():
		#print lis
	#print lista
	#exit(0)
	lista_da_passare = { 'id':'1234567:8568391'}
	lista_da_passare = {'contenttype': 'story', 'lead': {'picture': None, 'caption': '', 'link': 'http://www.rsi.ch/temp/test-push-to-social-Civi-8568391.html', 'name': 'test push to social Civi', 'description': 'subhead'}, 'title': 'test story title', 'lastmodifieddate': '2017-01-10T13:23:23Z', 'state': 'published', 'da_fare': [], 'id': 'article:8568391'}

	Controlla_Flag_Social( [ lista_da_passare])
	#print 'in main'
	#Prendi_Altro_Content( '8568391')
	exit(0)
	#programmeId  = GetSection_and_State( 117530 )
	#print programmeId
	#print 
	#print State_Id_xx
	#print
	#print Edited_Id_xx
